# mmOpBoilerplate

## Folder Structure

- src
	- app
		- components: project only components (footer, custom directives, etc..)
			- ...
		- mmCore: mmCore libraries
			- ...
		- pages: contain multiple folders (one for each page) with controller, services, templates, ...
			- main: main abstract route
				- app-init.service.js: app initialisation configuration (token, etc...)
				- ...
			- register: all you need for you register page
				- schema form: schema form configuration files
					- ...
				- register.controller.js: page controller
				- register.html: page template
				- register.routes.js: register routes config
				- register.scss: register scss rules
				- ...
		- services: custom global services
			- custom-endpoints.service.js: adds custom endpoints to $mmEndpoints
			- error-messages.service.js: form error messages
			- user-from-urlsparams.service.js: create a user object based on url params
			- ...
		- styles
			- factory: mmFactory styles and theme rules
				- ...
			- helpers.scss: list of custom helpers
			- variables.scss: bootstrap variables
			- ...
		- config.js: global app configuration
		- config.routes.js: global routing configuration
		- index.js: global app dependencies configuration
		- ...