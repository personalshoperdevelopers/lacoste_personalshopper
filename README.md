# Before install

`npm install -g nodemon`

# Install

`npm install && bower install`

# Run

- Local Dev: `gulp serve`
- Local Dev with build context: `gulp serve:dist`
- Local dev with api mock: `gulp serve:mock`
