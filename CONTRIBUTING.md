# How to contribute

## Clone the repository locally

`git clone git@gitlab.1000mercis.com:lib-front/mmOpBoilerplate.git`

## Styleguides

- Install SublimeLinter on your sublimetext
- Install eslint globally `npm install -g eslint`
- Install SublimeLinter-eslint on you sublimetext
- Follow those styleguides for JS ES5: https://github.com/airbnb/javascript/blob/master/es5
- Follow those styleguides for JS ES6: https://github.com/airbnb/javascript
- Follow those styleguides for css/sass: https://github.com/airbnb/css

## Git Flow

This project uses 'git-flow' workflow

- Never work on the 'master' branch
- All updates are merged onto the 'dev' branch
- Create a branch for each feature based on the 'dev' branch named like so 'feature/myfeature', then merge to develop when ready
- When the 'dev' branch is tested (manually for now) it will be merged on the 'master' branch and will be available for all when creating a new project.
