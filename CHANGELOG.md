## 1.7
### 09/04/2018

- [FIX] Correctif d'optin auto
- [FIX] Captcha
- [ADD] Integration de sentry

## 1.6
### 27/09/2016

- [ADD] Ajout d’une page d’erreur en cas d’erreur sur le token
- [ADD] Ajout de ui-mask dans les templates schema form
- [ADD] Ajout de bind-html-compile dans les templates schema form
- [ADD] Ajout de rtgconversion géré automatiquement et parametrable de app.constant.js
- [ADD] Ajout whatsapp par defaut dans le viral
- [ADD] Ajout des balises OG pour les share manuels (comentés)
- [FIX] AppConfig dans summary viral component
- [FIX] lien de desabo
- [FIX] redirections avec parametres d’urls qui perdent les params
- [FIX] formulaires de home qui s’entre-valident
- [FIX] nom du params dans l’url de dedup email
- [FIX] liens des templates des modals de viral
- [FIX] et refactorisation de l’envoie des emails de viral et messages d’erreurs
- [FIX] fermeture pop cookies
- [FIX] track click avec nouvelles regles sur le FB connect
- [FIX] relog FB Connect
- [FIX] décalage emails dans summary viral
- [IMPROVED] Refactorisation du appinit et du register auto
- [IMPROVED] Plus de hashbang (#!) dans les urls
- [IMPROVED] Clean dans les directives qui ne sont plus utilisées
- [IMPROVED] Reglement maintenant en pdf

### 25/07/2016

- Passage à Angular 1.5 (precedemement sur 1.4)
- Utilisation de composants à la place des templates/controllers actuels
- Passage à UiRouter 1.0 (attention les event StateChanceStart etc… vont disparaitre)
- Refactorisation complete de mmDoorman (restrictions d’accès coté front)
- Refactorisation complete des intercepteurs HTTP (renouvellement du token, relance d’une requette avec token expiré)
- La page Register disparait, il y a maintenant une page home avec des composants de formulaire signup/relog activables ou non
- Optinisation 1 click / register auto mis en place par default (code à décomenter)
- Un nouveau fichier de constantes regroupe toutes les principales infos d’une op en 1 seul endroit
- Moins de magie, mmCore disparait, le code à été nettoyé et directement intégré dans le boilerplate
- $mmEndpoint disparait, $resource est utilisé directement afin de facilité la compréhension des échanges front/back
- Parametre 'trotro-id’ sur la directive de track-click pour déclencher un event rtgtclickid mmtro
- Moins de conversion de nomage, token est maintenant utilisé nativement avec ‘_id’ plutot que de le convertir en ‘token’
- Plus de console.log mais des $log.degub désactivés en prod
- Utilisation dun attribu data.nextState sur la route pour specifier la page suivante sur la home

### 03/05/2016

**mm-cli et mmOpBoilerplate c'est quoi ?**

**mmOpBoilerplate :** Templates et structure de projet Slush-mm + mmCore fusionnés

**mm-cli :** Clone mmOpBoilerplate dans un dossier local pour débuter un nouveau projet (slush-mm sans les templates et sans les questions).

**Qu'est-ce qui change ?**

- Plus besoin de mettre à jour le générateur à chaque évolution/correctif (les sources ne sont plus hébergées en local chez les devs, mm-cli récupère directement la dernière version sur gitlab et les clones en local, il n'y a plus de différence de versions entre les devs).
- mmCore est directement inclus en dur dans les OP générées (on peut donc modifier les sources à volonté en cas de besoins spécifiques ou en cas de bug).
- Evolutions du boilerplate facilités, ce changement va nous permettre de:
- Travailler directement sur le boilerplate comme sur un projet lambda
- Tester et implémenter de nouvelles fonctionnalités Back/Fronts en conditions réelles en se branchant sur une API de dev
- Lancer des tests automatiques via Gitlab CI
- Le format de projet par default de ce boilerplate est... "factory/bundle, register + viral" et inclu les formulaires "material"

# Pre Boilerplate (slush-mm & mmCore)

### 17/02/2016

- Angular 1.3.7 à 1.4.3
- Angular UI Bootstrap 0.12.0 à 1.1.1 (règle les bugs de scroll de pop sur mobile et de certains bugs sur le carousel)
- Update des packages node et du gulp
- Fichier de variables bootstrap dans le dossier styles
- Pop réalisation avec bouton de fermeture par default
- Refonte du AppInit
- CHECKLIST dans le readme du projet
- Question rajoutée dans le générateur pour choisir les templates de pages à copier
- Routes de chaque page dans le dossier de la page pour copie/déplacement

### 22-10-2015

- Directive stop-event (mmCore): Utilisée par défaut sur les modals du footer et du summary viral, règle un bug qui empêchait de cliquer ou de se mettre en focus dans un champ de formulaire dans les modals.

- Directive check-dedup (fichier register.js du template slush-mm): La directive remplie maintenant automatiquement la valuer de l'optin marque en fonction des optins présent en dedup (optin et en dedup = 3).

- ng-model-option="{debounce: 500}" sur l'email de la page register (template slush-mm): Ajoute un delay de 500ms au check dedup sur la page register pour éviter d'envoyer trop de requêtes au back au moment ou l'on tape notre email.

info/countries (mmCore & slush-mm): Ajout du endpoint $mmEndpoints.info.countries pour récupérer les ref pays du back, et modification du sélecteur de pays dans register.html dans le template slush-mm pour envoyer le code INSEE à la place de l'id pays.

- Update des infos envoyées à l'appel du token (slush-mm fichier app-init.service.js):
  - oUrlParams: envoie la liste des paramètres d'url à l'arrivée sur l'op au back.
  - iUeId (param idue): present dans les urls des emails de relance envoyé via les pages de viral
  - iParrainId (param idup): id parrain présent dans les urls des share FB et twitter

- Service UserFromParams (dossier services dans le template slush-mm): Permet de creer un objet User à partir des paramètre d'urls (déjà dispo en dur dans l'ancienne version du générateur, maintenant dispo en tant que service).

- Filtre capitalize: Ajout du filtre capitalize pour mettre en cap la premiere lettre d'un mot, dans un controller ou directement dans un template (déjà dispo en dur dans l'ancienne version du générateur notamment utilisé pour les nom et prénom dans les params d'urls).

### 18/08/2015

**Fix:**
- Page de viral min required emails
- Envoie de mails vides
- regex email
- ajout du registerOrRelog pour le register auto dans le app init
