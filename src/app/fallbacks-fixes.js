// ADD IE IN ATTR BASE ON USER AGENT
if (navigator.userAgent.indexOf('MSIE') > -1 || navigator.userAgent.indexOf('Trident') > -1){
    document.documentElement.setAttribute('data-useragent', 'IE');
}

// IE CONSOLE FIX
/* eslint-disable no-console */
(function(){if(!document.location.origin){document.location.origin=window.location.protocol+'//'+window.location.hostname+(window.location.port ? ':'+window.location.port:'');}
    var method;var noop=function(){};var methods=['assert','clear','count','debug',
        'dir','dirxml','error','exception','group','groupCollapsed','groupEnd','info','log','markTimeline',
        'profile','profileEnd','table','time','timeEnd','timeStamp','trace','warn'];
    var length = methods.length;var console = (window.console = window.console || {});
    while(length--){method=methods[length];if(!console[method]){console[method]=noop;}}}());
/* eslint-enable no-console */

// FIX ORIGIN FOR IE <= 9
if (!window.location.origin) { window.location.origin = window.location.protocol + '//' + window.location.hostname + (window.location.port ? ':' + window.location.port: ''); }

// URL REWRITING FOR NON HTML5 MODE, From: 'www.monop.com/?my=param' To: 'www.monop.com/#!/?my=param'
angular
    .module('mmOp')
    .run(function($rootScope, $location, $window){
        $rootScope.$on('$locationChangeStart', function(e){
            var baseUrl = $location.absUrl();
            if(baseUrl.indexOf('#!') === -1 && baseUrl.indexOf('?') !== -1 && !(window.history && window.history.pushState)){
                e.preventDefault();
                $window.location.href = '/#!/'+ document.location.search; // REWRITE URL & REDIRECT
            }
        });
    });

//fix bug uiMask sumsung
angular.module('mmOp')
    .run(['uiMaskConfig', function (uiMaskConfig) {
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        var android = /android/i.test(userAgent);
        if (android) {
            var index = uiMaskConfig.eventsToHandle.indexOf('input');
            uiMaskConfig.eventsToHandle.splice(index, 1);
        }
    }]);
