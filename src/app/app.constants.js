'use strict';

angular
    .module('mmOp')
    .constant('AppConfig', {

        // DECLIS
        declis: [
            {
                name: 'fr',
                urls: [
                    'localhost',
                    'xxx.st1-mm.com',
                    'xxx.mm-dev3.com'
                ],
                apiVersion: 'v1'
            }
        ],

        // ENDPOINTS API PREFIXES
        getApiPrefix: function() {
            // var decliName = this.getDecli();
            // var apiVersion = this.declis.filter(function (decli) { return decli.name === decliName; })[0].apiVersion;
            return 'https://api.early-birds.fr';
        },

        redirectUrl: '',

        // LINKS REFS
        links: {},

        // IDFROMS REFS
        idFroms: {
            shareFacebook: 2,
            shareTwitter: 3,
            copyLink: 4,
            relanceEmail: 5,
            shareWhatsapp: 6
        },

        // MMTRO CONVERSION CONDITIONS
        mmTroConversion: {
            from: 'app.home',
            to: 'app.viral'
        },

        // FACEBOOK CONFIG
        facebook: {
            activateSdk: false,
            appIds: {
                localhost: 'xx',
                dev: 'xx',
                staging: 'xx',
                prod: 'xx'
            },
            share: {
                name: '...',
                description: '...'
            }
        },

        // TWITTER
        twitter: {
            activateSdk: false,
            share: {
                text: '...'
            }
        },
        // WHATSAPP
        whatsapp: {
            activateSdk: false,
            share: {
                text: '...'
            }
        },

        // COOKIE BANNER
        cookies: {
            site: '...',
            client: '...'
        },

        // FOOTER
        footer: {
            contact: 'mail@mail.com',
            subject: 'Contact',
            charteFb: {
                client: '...'
            }
        },

        // GET ENV: 1 = DEV | 0 = PROD
        env: function() {
            var env = 0;
            var host = document.location.hostname;
            if (host.search(/(localhost)|(mm-dev3)|(st1-mm)/) > -1) {
                env = 1;
            }
            return env;
        },

        // GET LANG
        getLanguage: function() {
            // ex: if uk.xxx.com -> getLanguage returns uk
            if(typeof Lacoste != 'undefined'){
                if (Lacoste.siteprefs.CURRENT_LOCALE) {
                    if(Lacoste.siteprefs.CURRENT_LOCALE.toLowerCase() == 'fr' || Lacoste.siteprefs.CURRENT_LOCALE.toLowerCase() == 'fr_ch') return 'fr';
                    else if(Lacoste.siteprefs.CURRENT_LOCALE.toLowerCase() == 'en' || Lacoste.siteprefs.CURRENT_LOCALE.toLowerCase() == 'en_ch') return 'en';
                    else if(Lacoste.siteprefs.CURRENT_LOCALE.toLowerCase() == 'de' || Lacoste.siteprefs.CURRENT_LOCALE.toLowerCase() == 'de_ch') return 'de';
                    else return Lacoste.siteprefs.CURRENT_LOCALE;
                } else {
                    return 'en';
                }
            } else {
                return 'en';
            }
        },

        // GET DECLI
        getDecli: function() {
            var selectedDecli = this.declis.filter(function (decli) {
                return decli.urls.filter(function (url) {
                    return document.location.host.search(url) > -1;
                }).length > 0;
            })[0];

            return selectedDecli ? selectedDecli.name: this.declis[0].name;
        },

        // GET ID API EARLY BIRDS (en fonction du pays) -- EN ATTENTE DU JSON D'EB
        getIdApi: function(){
            var apiId = '5abe5e48c8a59210e5c4024d';
            return apiId;
        },

        // CHECK IF IN IFRAME
        inIframe: function() {
            try {
                return window.self !== window.top;
            } catch (e) {
                return true;
            }
        }
    });
