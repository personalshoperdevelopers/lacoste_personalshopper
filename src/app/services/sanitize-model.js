'use strict';

angular
    .module('mmOp')
    .factory('SanitizeModel', function ($sanitize) {
        return function(inputModel){
            var sanitizeModel = function (model) {
                return Object.keys(model).reduce(function(finalObj, key) {
                    if (typeof model[key] === 'object') {
                        finalObj[key] = sanitizeModel(model[key]);
                    } else if (typeof model[key] === 'string') {
                        finalObj[key] = $sanitize(model[key]);
                    } else {
                        finalObj[key] = model[key];
                    }
                    return finalObj;
                }, {});
            };
            return sanitizeModel(inputModel);
        };
    });
