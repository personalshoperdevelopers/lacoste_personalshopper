'use strict';

angular
    .module('mmOp')
    .factory('WidgetId', function () {
        return [
            {
                'fr': {
                    'looks':'5abe5e48c8a59210e5c4024d',
                    'master': '5ac32b33f31978243e347007',
                    'products': '5ac32ba3f31978243e34700a'
                }
            }
        ];
    });
