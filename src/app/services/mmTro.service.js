'use strict';

angular.module('mmOp')
  .factory('$mmTro', function($window, $q, $mmUser, AppConfig, $log, $stateParams) {

    var _tagid = "6553615-12c587ffd07fe58d89807bf8ec15be89";

    // Queue of params
    // ------------------------------------------------------------------------
    return {

      /**
       * Local queueing storing object
       * @type {Object}
       */
      queue: {},

      /**
       * Add data to $mmTro queue, waiting to be sent
       * @param {Object} oParams Object of params to store in queue
       * @return {Object} $mmTro service
       */
      addToQueue: function(oParams){
        if (!_tagid) { return; }

        /**
         * Create basic trotro param object then it add to the queue
         */
        var Troto = function(){
          this.tagid = _tagid;
          this._rtgenvironnement = 'Personal_Shopper';
          // this._rtgidfrom = $mmUser.get().fromId;
          // this._rtgdevenv = AppConfig.env();

          if ($mmUser.get().sEmail || $mmUser.get().Email || $stateParams.e) {
            this._email = $mmUser.get().sEmail || $mmUser.get().Email || $stateParams.e;
          }
          if ($mmUser.get().idU) {
            this._rtgidu = $mmUser.get().idU;
            this._exclusion = 1;

            if($mmUser.get().oResponses && $mmUser.get().oResponses.iQuestion_4) {
              this._rtgnewregistered = 0;
            }
            else {
              this._rtgnewregistered = 1;
            }
          }
          if ($mmUser.get().visitId) {
            this._rtgvisitid = $mmUser.get().visitId;
          }

          // OPTINS
          if ($mmUser.get().oOptins && $mmUser.get().oOptins.iOptin_2 != null) {
            this._rtgtypeconsentement2 = $mmUser.get().oOptins.iOptin_2;
          }
          if ($mmUser.get().oOptins && $mmUser.get().oOptins.iOptin_3 != null) {
            this._rtgtypeconsentement3 = $mmUser.get().oOptins.iOptin_3;
          }
          if ($mmUser.get().oOptins && $mmUser.get().oOptins.iOptin_4 != null) {
            this._rtgtypeconsentement4 = $mmUser.get().oOptins.iOptin_4;
          }
        };
        angular.extend(this.queue, oParams, new Troto());
        // $log.debug('$mmTro: ADD TO TROTRO QUEUE', this.queue, '\n\n');

        return this;
      },

      /**
       * Send queueing data to mmtro
       */
      sendRtg: function(){
        if (!_tagid) { return; }

        // IF window._troq EXIST
        if($window._troq){
          // $log.debug('$mmTro: SEND TO TROTRO', this.queue);

          var final = [];
          angular.forEach(this.queue, function(value, key){
            final.push([key, value]);
          });
          $window._troq.push.apply($window._troq, final);
        }

        // IF window._troq DOESNT EXIST -> INIT TROTRO
        else{
          // $log.debug('$mmTro: INIT & SEND TO TROTRO', this.queue);
          $window._troq = $window._troq || [];
          $window._troq.push(['tagid', _tagid]);

          angular.forEach(this.queue, function(value, key){
            $window._troq.push([key, value]);
          });

          (function() {
            var ts = document.createElement('script');
            ts.type = 'text/javascript';
            ts.async = true;
            ts.src = '//mmtro.com/tro.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ts, s);
          })();
        }

        this.queue = {};
        // $log.debug('$mmTro: RESET TROTRO QUEUE', this.queue);
      }
    };
  });

  // DYNAMICALLY ADDS _rtgpg
  angular.module('mmOp')
    .run(function($rootScope, $mmTro, $transitions) {
      $transitions.onSuccess({}, function(trans){
        var state = trans.promise.$$state.value;

        if (state.data && state.data.pageId) {
          $mmTro.addToQueue({ _rtgpg: state.data.pageId });
        } else {
          $mmTro.addToQueue();
        }
      });
     });
