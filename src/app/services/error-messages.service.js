'use strict';

angular
    .module('mmOp')
    .factory('$mmErrorMsg', function () {
        return {
            fr: {
                'iCivilId': {
                    '302': 'Veuillez renseigner votre civilité.'
                },

                'sFirstName': {
                    '202': 'Veuillez renseigner un format de prénom correct.',
                    '302': 'Veuillez renseigner votre prénom.'
                },

                'sLastName': {
                    '202': 'Veuillez renseigner un format de nom correct.',
                    '302': 'Veuillez renseigner votre nom.'
                },

                'sEmail': {
                    '101': 'Veuillez renseigner au moins un email.',
                    '202': 'Veuillez renseigner un format d\'email correct.',
                    '302': 'Veuillez renseigner votre email.',
                    'notRegistered': 'Cet email n\'est pas inscrit',
                    'alreadyRegistered': 'Cet email est déjà inscrit. Pour vous reconnecter, cliquez sur " déjà inscrit(e) ?"',
          			'sameAs': 'Vous avez au moins deux emails identiques.',
                    'viralError1': 'Vous ne pouvez pas inviter votre propre adresse.',
                    'viralError2': 'Cet email est déjà invité.',
                    'viralError3': 'Cet email est déjà inscrit.'
                },

                'sMobile': {
                    '202': 'Veuillez renseigner un format de mobile correct.',
                    '302': 'Veuillez renseigner votre numéro de mobile.'
                },

                'iZipcode': {
                    '202': 'Veuillez renseigner un format de code postal correct.',
                    '302': 'Veuillez renseigner votre code postal.'
                },

                'iCountryId': {
                    '302': 'Veuillez renseigner votre pays.'
                },

                'sPassword': {
                    '302': 'Veuillez saisir un mot de passe.',
                    'sameAs': 'La confirmation du mot de passe ne correspond pas.'
                }
            }
        };
    });
