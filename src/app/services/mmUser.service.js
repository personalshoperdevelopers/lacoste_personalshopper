'use strict';

angular
    .module('mmOp')
    .factory('$mmUser', function($window, $sessionStorage, $log){
        var localSessionStorage = {};

        return {
            set: function(dataSet){
                angular.forEach(dataSet, function(dataValue, dataKey){

                    if(dataValue !== undefined && dataValue !== null){

                        localSessionStorage[dataKey] = dataValue;

                        // IF SESSION STORAGE DUPLICATE IN SESSION STORAGE
                        try {
                            $sessionStorage[dataKey] = dataValue;
                        } catch (e) {
                            $log.debug(e);
                        }
                    }
                });

                Raven.setExtraContext({
                    user: {
                        _id: localSessionStorage._id,
                        fromId: localSessionStorage.fromId,
                        iOpStatus: localSessionStorage.iOpStatus,
                        visitId: localSessionStorage.visitId,
                        uid: localSessionStorage.uid,
                        sEmail: localSessionStorage.sEmail
                    }
                });

                return this;
            },
            get: function(){
                try {
                    angular.forEach($sessionStorage, function(dataValue, dataKey){
                        localSessionStorage[dataKey] = dataValue;
                    });
                } catch (e) {
                    $log.debug(e);
                }

                return localSessionStorage;
            },
            remove: function(keysArray){
                for(var key in keysArray){
                    delete localSessionStorage[keysArray[key]];

                    // IF SESSION STORAGE DUPLICATE IN SESSION STORAGE
                    try {
                        delete $sessionStorage[keysArray[key]];
                    } catch (e) {
                        $log.debug(e);
                    }
                }
            }
        };
    });
