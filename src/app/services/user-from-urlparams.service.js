'use strict';

angular
    .module('mmOp')
    .factory('UserFromParams', function ($filter) {
        return function(stateParams){

            var User = function(stateParams){
                stateParams.e  ? this.sEmail     = stateParams.e                        : null;
                stateParams.p  ? this.sFirstName = $filter('capitalize')(stateParams.p) : null;
                stateParams.n  ? this.sLastName  = $filter('capitalize')(stateParams.n) : null;
                stateParams.c  ? this.iCivilId   = parseInt(stateParams.c)              : null;
                stateParams.cp ? this.iZipcode   = stateParams.cp                       : null;

                this.oResponses = {};
                this.oOptins = {};

                if(stateParams.bd){
                    this.setDateBd(stateParams.bd);
                }

                stateParams.idbo  ? this.oResponses.sIDBO_2 = stateParams.idbo : null;
            };
            /**
       * Methode de l'objet user qui map les paramettre url de type String yyyy{\/|-|other separator}mm{\/|-|other separator}dd
       * pour sortir une date en format dd/mm/yyyy {fr-FR} de type string
       * @param dbParams
       * @returns {*}
       */
            User.prototype.setDateBd = function (dbParams) {
                if (!dbParams) return null;

                var bd = new Date(dbParams);
                var options = {
                    day: 'numeric', month: 'numeric', year: 'numeric',
                    hour12: false
                };

                return this.oResponses.dBirthDate_1 = bd.toLocaleString('fr-FR', options);
            };

            return new User(stateParams);
        };
    });
