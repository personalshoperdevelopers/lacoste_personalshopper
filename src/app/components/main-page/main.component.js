'use strict';

angular
    .module('mmOp')
    .component('mainPage', {
        templateUrl: 'app/components/main-page/main.html',
        controller: function ($rootScope, $translate, AppConfig) {
            var $ctrl = this;

            $translate.use(AppConfig.getLanguage());

            $rootScope.currentPage = 1;

            $rootScope.$on('updatePage', function(e, obj){
            	$rootScope.currentPage = obj.idPage;
            });

        }
    });
