'use strict';

// MAIN ABSTRACT ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
angular
    .module('mmOp')
    .config(function ($stateProvider) {

        $stateProvider
            .state({
                name: 'app',
                url: '/',
                abstract: true,
                component: 'mainPage'
            });
    });
