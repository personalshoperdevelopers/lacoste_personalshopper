'use strict';

angular
    .module('mmOp')
    .component('loadingPage', {
        templateUrl: 'app/components/loading-page/loading.html',
        controller: function ($state, $mmUser, AppConfig, $resource, $timeout, ImagesData, WidgetId, $rootScope) {
            var $ctrl = this;

            // Taggage /////////////////////////

            var currentLang = AppConfig.getLanguage();
            var today = new Date();
            today = today.toLocaleString().split(',');
            var currentDate = today[0] + '' + today[1];

            var aGenderTaggage = ['Men', 'Women', 'Boys', 'Girls'];
            var aEventTaggage = ['AllOccasions', 'Weekend', 'Sport', 'City'];

            var genderTaggage = aGenderTaggage[parseInt($mmUser.get().genderId)-1];
            var eventTaggage = aEventTaggage[parseInt($mmUser.get().occasionId)-1];

            var utag_data = {
                page: {
                    language: currentLang,
                    pagecategory: 'Personnal Shopper',
                    pagename: 'Personnal Shopper/Waiting Page',
                    pageurl: 'https://www.lacoste.com/'+currentLang+'/',
                    sitecountrycode: currentLang.toUpperCase(),
                    sitecountryname: (currentLang == 'gb') ? '' : 'France',
                    subcategory: 'Waiting Page',
                    visitdate: currentDate // à voir format...
                },
                user: {
                    internalId: 'guest',
                    loggedin: 'false',
                    newsletterOptin: 'false',
                    usercountrycode: currentLang.toUpperCase(),
                    usercountryname: (currentLang == 'gb') ? '' : 'France',
                    usertype: 'guest'
                },
                personnalshopper: {
                    step: 4,
                    genderSelected: genderTaggage,
                    occasionselected: eventTaggage,
                    product: {}
                }
            }
            window.utag_data = utag_data;
            if(typeof Bootstrapper !== 'undefined') Bootstrapper.ensEvent.trigger("personnalShopper_changestep");
            ///////////////////////////////////

            var widgetId = WidgetId[0]['fr'];

            var profileId = ($mmUser.get().profile) ? $mmUser.get().profile.id : '';

            $ctrl.objImages = ImagesData[0].loadingImg;

            $timeout(function(){

                var _speed = 0.15;
                var tlLoader = new TimelineMax({repeat:-1, repeatDelay: _speed});

                var listImg = angular.element('.mm-loader li');
                var aTweens = [];

                TweenMax.to(angular.element(listImg[0]), 0, {display:'block', delay:_speed*i});

                for(var i = 0; i < listImg.length; i++) aTweens.push(TweenMax.to(angular.element(listImg[i]), 0, {display:'block', delay:_speed*i}));

                tlLoader.add(aTweens);

                $timeout(function(){
                    var paramId = widgetId.master;
                    $resource(AppConfig.getApiPrefix()+'/widget/'+ paramId +'/recommendations/'+ profileId)
                        .get({
                            variables:{
                                '$gender': $mmUser.get().gender,
                                '$occasion': $mmUser.get().occasion
                            }
                        })
                        .$promise
                        .then(function(result){
                            $mmUser.set({
                                'mainLook':result
                            });
                            $rootScope.$broadcast('updatePage', {idPage:4});
                        }).catch(function(error){

                        });

                }, 1500);
        
            });
        }
    });
