'use strict';

angular
    .module('mmOp')
    .component('savedLooks', {
        templateUrl: 'app/components/menu-component/saved-looks/saved-looks.html',
        controller: function (AppConfig, $mmUser, $resource, $rootScope, WidgetId) {
         
            var $ctrl = this;

            $ctrl.allLookSaved = $mmUser.get().aLooks;

            $rootScope.$on('updateLookSaved', function(){
                $ctrl.allLookSaved = $mmUser.get().aLooks;
            });
            
            $ctrl.deleteLook = function(indexLook){
                var aLooks = $mmUser.get().aLooks;
                aLooks.splice(indexLook, 1);
                $mmUser.set({
                    aLooks: aLooks
                });
            };

            $ctrl.loadLook = function(indexLook){
                $rootScope.$broadcast('loadSavedLook', indexLook);
            }

        }
    });
