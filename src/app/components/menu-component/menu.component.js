'use strict';

angular
    .module('mmOp')
    .component('mmMenu', {
        templateUrl: 'app/components/menu-component/menu.html',
        controller: function ($rootScope, $scope, $mmUser, ImagesData, $state, $translate, $timeout, WidgetId, $resource, AppConfig) {

            var $ctrl = this;

            // CHOICE GENDER/EVENT //

            $timeout(function(){

                $ctrl.gender = $mmUser.get().genderId;
                $ctrl.event = $mmUser.get().occasionId;

                if($mmUser.get().genderId == 3 || $mmUser.get().genderId == 4) $ctrl.event = 1;

                var aGender = ['man', 'woman', 'boy', 'girl'];
                var aEvent = ['whenever', 'weekend', 'sport', 'urban'];

                var aGenderLabel = [];
                var aEventLabel = [];

                var widgetId = WidgetId[0]['fr']; // à remplacer par la bonne langue quand on aura les Ids...
                var profileId = ($mmUser.get().profile) ? $mmUser.get().profile.id : '';



                $ctrl.isMin = (window.outerWidth < 992) ? true : false;
                $ctrl.isFilterOpen = ($ctrl.isMin) ? false : true;

                angular.element(window).resize(function(){
                    $ctrl.isMin = (window.outerWidth < 992) ? true : false;
                    $ctrl.isFilterOpen = ($ctrl.isMin) ? false : true;
                    $scope.$apply();
                });

                angular.element(window).resize(function(){
                    $ctrl.isMin = (window.outerWidth < 992) ? true : false;
                    $ctrl.isFilterOpen = ($ctrl.isMin) ? false : true;
                    $scope.$apply();
                });

                $ctrl.openFilters = function(){
                    $ctrl.isFilterOpen = !$ctrl.isFilterOpen;
                }

                $ctrl.closeFilters = function(){
                    $ctrl.isFilterOpen = false;
                }

                $translate(['home.gender_1', 'home.gender_2', 'home.gender_3', 'home.gender_4', 'home.event_1', 'home.event_2', 'home.event_3', 'home.event_4']).then(function(result){
                    
                    aGenderLabel[0] = result['home.gender_1'];
                    aGenderLabel[1] = result['home.gender_2'];
                    aGenderLabel[2] = result['home.gender_3'];
                    aGenderLabel[3] = result['home.gender_4'];

                    aEventLabel[0] = result['home.event_1'];
                    aEventLabel[1] = result['home.event_2'];
                    aEventLabel[2] = result['home.event_3'];
                    aEventLabel[3] = result['home.event_4'];

                    $ctrl.genderLabel = aGenderLabel[($ctrl.gender-1)];
                    $ctrl.eventLabel = aEventLabel[($ctrl.event-1)];

                    $mmUser.set({
                        gender: aGender[($ctrl.gender-1)],
                        genderId: $ctrl.gender,
                        occasion: aEvent[($ctrl.event-1)],
                        occasionId: $ctrl.event
                    });

                    $rootScope.$broadcast('updateMenu');

                    $ctrl.clickGenderChoice = function(){
                        if($ctrl.event) $ctrl.gender = null;
                    };

                    $ctrl.updateChoiceMenu = function(id){
                        $ctrl.gender = id;
                        $ctrl.genderLabel = aGenderLabel[($ctrl.gender-1)];

                        $mmUser.set({
                            genderId: $ctrl.gender
                        });
                        $rootScope.currentLook = 1;

                        $rootScope.posHeadAccessories = 0;
                        $rootScope.posBig = 0;
                        $rootScope.posTop = 0;
                        $rootScope.posBottom = 0;
                        $rootScope.posAccessories = 0;
                        $rootScope.posShoes = 0;
                        $rootScope.posFull = 0;

                        $rootScope.$broadcast('updateMenu');
                        updateAllChoices();
                    };

                    $ctrl.clickEventChoice = function(){
                        $ctrl.event = null;
                    };

                    $ctrl.updateChoiceMenuEvent = function(id){
                        $ctrl.event = id;
                        $ctrl.eventLabel = aEventLabel[($ctrl.event-1)];

                        $mmUser.set({
                            occasionId: $ctrl.event
                        });                    
                        $rootScope.currentLook = 1;

                        $rootScope.posHeadAccessories = 0;
                        $rootScope.posBig = 0;
                        $rootScope.posTop = 0;
                        $rootScope.posBottom = 0;
                        $rootScope.posAccessories = 0;
                        $rootScope.posShoes = 0;
                        $rootScope.posFull = 0;

                        $rootScope.$broadcast('updateMenu');
                        updateAllChoices();
                    };

                    var filterCollection = [];
                    var filterProducts = [];

                    function updateAllChoices(){

                        $rootScope.currentTypeLook = 'top';

                        if($mmUser.get().genderId == 3 || $mmUser.get().genderId == 4) $ctrl.event = 1;

                        $mmUser.set({
                            gender: aGender[($ctrl.gender-1)],
                            genderId: $ctrl.gender,
                            occasion: aEvent[($ctrl.event-1)],
                            occasionId: $ctrl.event
                        });

                        var paramId = widgetId.looks;

                        $resource(AppConfig.getApiPrefix()+'/widget/'+ paramId + '/recommendations/' + profileId )
                        .get({
                            variables:{
                                '$gender': aGender[($ctrl.gender-1)],
                                '$occasion': aEvent[($ctrl.event-1)],
                                '$universes': filterCollection,
                                '$product_lines': filterProducts
                            }
                        })
                        .$promise
                        .then(function(result){
                            $mmUser.set(result);
                            // $state.go('app.qualif');
                            // $rootScope.$broadcast('updatePage', {idPage:4});
                            var _obj = {
                                            '$gender': $mmUser.get().gender,
                                            '$occasion': $mmUser.get().occasion
                                        }
                            if(filterCollection.length > 0)  _obj['$universes'] = filterCollection;
                            if(filterProducts.length > 0)  _obj['$product_lines'] = filterProducts;
                            
                            $timeout(function(){
                                var paramId = widgetId.master;
                                $resource(AppConfig.getApiPrefix()+'/widget/'+ paramId +'/recommendations/'+ profileId)
                                .get({
                                    variables: _obj
                                })
                                .$promise
                                .then(function(result){
                                    $mmUser.set({
                                        'mainLook':result
                                    });
                                    $rootScope.$broadcast('masterProductChange', $mmUser.get().mainLook.recommendations[$rootScope.currentLook - 1], filterCollection);

                                }).catch(function(error){

                                });

                            }, 200);
                        }).catch(function(error){
                            
                        });

                    };

                    // FILTERS

                    $ctrl.aFilters = [false, false, false, false]; //0: LACOSTE, 1: LACOSTE LIVE, 2: LACOSTE SPORT, 3: COLLECTION DÉFILÉ
                    $ctrl.aFiltersProducts = [false, false, false, false, false, false, false, false, false]; //0: POLO, 1: T-Shirt, 2: Chemise, 3: Robe, 4: pantalons, 5: survet, 6: short, 7: jupes, 8: hauts

                    var aGenderTaggage = ['Men', 'Women', 'Boys', 'Girls'];
                    var aEventTaggage = ['AllOccasions', 'Weekend', 'Sport', 'City'];

                    $ctrl.genderTaggage = aGenderTaggage[parseInt($mmUser.get().genderId)-1];
                    $ctrl.eventTaggage = aEventTaggage[parseInt($mmUser.get().occasionId)-1];

                    $ctrl.objImages = ImagesData[0];

                    $ctrl.savedLooks = JSON.parse(sessionStorage.getItem('savedLooks')) || [];
              
                    $rootScope.currentLook = 1;
                    
                    if($mmUser.get().mainLook){
                        $rootScope.currentLookId = ($mmUser.get().mainLook.recommendations[($rootScope.currentLook-1)]) ? $mmUser.get().mainLook.recommendations[($rootScope.currentLook-1)].id : null;
                    }

                    $rootScope.$on('updateFiltersCollection', function(){
                        $rootScope.currentLook = 1;
                    });

                    $rootScope.$on('updateFiltersProducts', function(){
                        $rootScope.currentLook = 1;
                    });
                    
                    $rootScope.$on('updateLook', function(e, currentLookId){
                        $ctrl.genderTaggage = aGenderTaggage[parseInt($mmUser.get().genderId)-1];
                        $ctrl.eventTaggage = aEventTaggage[parseInt($mmUser.get().occasionId)-1];
                        $rootScope.currentLookId = currentLookId;
                    })
                    $rootScope.$on('deleteSavedLook', function(e, index){
                        $ctrl.savedLooks.splice(index, 1);
                        sessionStorage.setItem('savedLooks', JSON.stringify($ctrl.savedLooks));
                    })

                    $ctrl.prevLook = function(){
                        $rootScope.currentTypeLook = 'top';
                        $rootScope.currentLook = ($rootScope.currentLook == 1) ? 5 : $rootScope.currentLook - 1;
                        $rootScope.$broadcast('masterProductChange', $mmUser.get().mainLook.recommendations[$rootScope.currentLook - 1]);
                    };

                    $ctrl.isFilterCheck = function(){
                        var isCheck = false;
                        var _aFilters = $ctrl.aFilters.filter(function(row){
                            return row == true
                        });
                        if(_aFilters.length > 0) isCheck = true;
                        else isCheck = false;
                        return isCheck;
                    }

                    $ctrl.isFilterProductsCheck = function(){
                        var isCheck = false;
                        var _aFilters = $ctrl.aFiltersProducts.filter(function(row){
                            return row == true
                        });
                        if(_aFilters.length > 0) isCheck = true;
                        else isCheck = false;
                        return isCheck;
                    }


                    $rootScope.$on('prevLookFull', $ctrl.prevLook);

                    $ctrl.nextLook = function(){
                        $rootScope.currentTypeLook = 'top';
                        $rootScope.currentLook = ($rootScope.currentLook == 5) ? 1 : $rootScope.currentLook + 1;
                        $rootScope.$broadcast('masterProductChange', $mmUser.get().mainLook.recommendations[$rootScope.currentLook - 1]);
                    };

                    $rootScope.$on('nextLookFull', $ctrl.nextLook);

                    $ctrl.updateFiltersCollection = function(e){
                        $rootScope.$broadcast('updateFiltersCollection', {aUniverses: $ctrl.aFilters})
                    }

                    $ctrl.updateFiltersProducts = function(e){
                        $rootScope.$broadcast('updateFiltersProducts', {aProductLines: $ctrl.aFiltersProducts})
                    }

                    // SAVE LOOK
                    $ctrl.aLooks = ($mmUser.get().aLooks) ? $mmUser.get().aLooks : [];

                    $rootScope.posHeadAccessories = 0;
                    $rootScope.posBig = 0;
                    $rootScope.posTop = 0;
                    $rootScope.posBottom = 0;
                    $rootScope.posAccessories = 0;
                    $rootScope.posShoes = 0;
                    $rootScope.posFull = 0;

                    $ctrl.saveLook = function(){
                        var allProducts = $mmUser.get().allProducts;
                        var ui1 = (allProducts.headAccessories[$rootScope.posHeadAccessories]) ? allProducts.headAccessories[$rootScope.posHeadAccessories]._id.original_id : '';
                        var ui2 = (allProducts.big[$rootScope.posBig]) ? allProducts.big[$rootScope.posBig]._id.original_id : '';
                        var ui3 = (allProducts.top[$rootScope.posTop]) ? allProducts.top[$rootScope.posTop]._id.original_id : '';
                        var ui4 = (allProducts.bottom[$rootScope.posBottom]) ? allProducts.bottom[$rootScope.posBottom]._id.original_id : '';
                        var ui5 = (allProducts.accessories[$rootScope.posAccessories]) ? allProducts.accessories[$rootScope.posAccessories]._id.original_id : '';
                        var ui6 = (allProducts.shoes[$rootScope.posShoes]) ? allProducts.shoes[$rootScope.posShoes]._id.original_id : '';
                        var ui7 = (allProducts.full[$rootScope.posFull]) ? allProducts.full[$rootScope.posFull]._id.original_id : '';

                        var _filterCollection = null;
                        if($ctrl.aFilters[0]) _filterCollection = 'SPW';
                        if($ctrl.aFilters[1]) _filterCollection = 'LIV';
                        if($ctrl.aFilters[2]) _filterCollection = 'SPO';
                        if($ctrl.aFilters[3]) _filterCollection = 'FAS';

                        var _filterProducts = null;
                        if($ctrl.aFiltersProducts[0]) _filterProducts = '1P'; // POLOS
                        if($ctrl.aFiltersProducts[1]) _filterProducts = '1T'; // T-SHIRT
                        if($ctrl.aFiltersProducts[2]) _filterProducts = '1C'; // SHIRTS
                        if($ctrl.aFiltersProducts[3]) _filterProducts = '1E'; // DRESSES
                        if($ctrl.aFiltersProducts[4]) _filterProducts = '1H'; // PANTALONS
                        if($ctrl.aFiltersProducts[5]) _filterProducts = '1W'; // SURVÊT
                        if($ctrl.aFiltersProducts[6]) _filterProducts = '1G'; // SHORTS
                        if($ctrl.aFiltersProducts[7]) _filterProducts = '1J'; // JUPES
                        if($ctrl.aFiltersProducts[8]) _filterProducts = '1Q'; // TOPS

                        var objLook = {
                            uniqueId: ui1+'_'+ui2+'_'+ui3+'_'+ui4+'_'+ui5+'_'+ui6+'_'+ui7,
                            idMainLook: $rootScope.currentLook,
                            genderId: $ctrl.gender,
                            eventId: $ctrl.event,
                            posHA: $rootScope.posHeadAccessories,
                            posBig: $rootScope.posBig,
                            posTop: $rootScope.posTop,
                            posBottom: $rootScope.posBottom,
                            posA: $rootScope.posAccessories,
                            posShoes: $rootScope.posShoes,
                            posFull: $rootScope.posFull,
                            headAccessory: allProducts.headAccessories[$rootScope.posHeadAccessories],
                            big: allProducts.big[$rootScope.posBig],
                            top: allProducts.top[$rootScope.posTop],
                            bottom: allProducts.bottom[$rootScope.posBottom],
                            accessories: allProducts.accessories[$rootScope.posAccessories],
                            shoes: allProducts.shoes[$rootScope.posShoes],
                            full: allProducts.full[$rootScope.posFull],
                            filterCollection: _filterCollection,
                            filterProducts: _filterProducts
                        }

                        if(!$ctrl.checkLookAlreadyExist()){
                            $ctrl.aLooks.push(objLook);
                            $mmUser.set({
                                aLooks: $ctrl.aLooks
                            });
                            $rootScope.$broadcast('updateLookSaved');
                        }
                    };

                    $ctrl.checkLookAlreadyExist = function(){
                        var allProducts = $mmUser.get().allProducts;
                        if(typeof allProducts != 'undefined'){
                            var ui1 = (allProducts.headAccessories[$rootScope.posHeadAccessories]) ? allProducts.headAccessories[$rootScope.posHeadAccessories]._id.original_id : '';
                            var ui2 = (allProducts.big[$rootScope.posBig]) ? allProducts.big[$rootScope.posBig]._id.original_id : '';
                            var ui3 = (allProducts.top[$rootScope.posTop]) ? allProducts.top[$rootScope.posTop]._id.original_id : '';
                            var ui4 = (allProducts.bottom[$rootScope.posBottom]) ? allProducts.bottom[$rootScope.posBottom]._id.original_id : '';
                            var ui5 = (allProducts.accessories[$rootScope.posAccessories]) ? allProducts.accessories[$rootScope.posAccessories]._id.original_id : '';
                            var ui6 = (allProducts.shoes[$rootScope.posShoes]) ? allProducts.shoes[$rootScope.posShoes]._id.original_id : '';
                            var ui7 = (allProducts.full[$rootScope.posFull]) ? allProducts.full[$rootScope.posFull]._id.original_id : '';
                            var uniqueId = ui1+'_'+ui2+'_'+ui3+'_'+ui4+'_'+ui5+'_'+ui6+'_'+ui7;
                            
                            var lookAlreadySaved = $ctrl.aLooks.filter(function(row){
                                return row.uniqueId == uniqueId
                            });
                            if(lookAlreadySaved.length > 0 && $ctrl.aLooks.length <= 5){ // can save only 5 looks
                                $rootScope.currentSavedLookId = uniqueId;
                                return true;
                            } else {
                                $rootScope.currentSavedLookId = '';
                                return false;
                            }
                        }
                    }

                    // LOAD LOOK
                    $rootScope.$on('loadSavedLook', function(e, indexSavedLook){
                        var lookToLoad = $ctrl.aLooks[indexSavedLook];
                        if($ctrl.gender != lookToLoad.genderId){
                            $ctrl.gender = lookToLoad.genderId;
                            $ctrl.genderLabel = aGenderLabel[($ctrl.gender-1)];

                            $mmUser.set({
                                genderId: $ctrl.gender
                            });
                        }
                        if($ctrl.event != lookToLoad.eventId){
                            $ctrl.event = lookToLoad.eventId;
                            $ctrl.eventLabel = aEventLabel[($ctrl.event-1)];

                            $mmUser.set({
                                occasionId: $ctrl.event
                            });
                        }

                        $rootScope.$broadcast('updateMenu');

                        // Filters collection
                        $ctrl.aFilters = [false, false, false, false];
                        if(lookToLoad.filterCollection == 'SPW') $ctrl.aFilters[0] = true;
                        if(lookToLoad.filterCollection == 'LIV') $ctrl.aFilters[1] = true;
                        if(lookToLoad.filterCollection == 'SPO') $ctrl.aFilters[2] = true;
                        if(lookToLoad.filterCollection == 'FAS') $ctrl.aFilters[3] = true;
                        filterCollection = [];
                        if(lookToLoad.filterCollection != null) filterCollection.push(lookToLoad.filterCollection);

                        // Filters products
                        $ctrl.aFiltersProducts = [false, false, false, false, false, false, false, false, false]; //0: POLO, 1: T-Shirt, 2: Chemise, 3: Robe, 4: pantalons, 5: survet, 6: short, 7: jupes, 8: hauts
                        

                        if(lookToLoad.filterProducts == '1P') $ctrl.aFiltersProducts[0] = true;
                        if(lookToLoad.filterProducts == '1T') $ctrl.aFiltersProducts[1] = true;
                        if(lookToLoad.filterProducts == '1C') $ctrl.aFiltersProducts[2] = true;
                        if(lookToLoad.filterProducts == '1E') $ctrl.aFiltersProducts[3] = true;
                        if(lookToLoad.filterProducts == '1H') $ctrl.aFiltersProducts[4] = true;
                        if(lookToLoad.filterProducts == '1W') $ctrl.aFiltersProducts[5] = true;
                        if(lookToLoad.filterProducts == '1G') $ctrl.aFiltersProducts[6] = true;
                        if(lookToLoad.filterProducts == '1J') $ctrl.aFiltersProducts[7] = true;
                        if(lookToLoad.filterProducts == '1Q') $ctrl.aFiltersProducts[8] = true;
                        filterProducts = [];
                        if(lookToLoad.filterProducts != null) filterProducts.push(lookToLoad.filterProducts);

                        updateAllChoices();

                        $rootScope.currentLook = lookToLoad.idMainLook;

                        // $timeout(function(){
                            $rootScope.posHeadAccessories = lookToLoad.posHA;
                            $rootScope.posBig = lookToLoad.posBig;
                            $rootScope.posTop = lookToLoad.posTop;
                            $rootScope.posBottom = lookToLoad.posBottom;
                            $rootScope.posAccessories = lookToLoad.posA;
                            $rootScope.posShoes = lookToLoad.posShoes;
                            $rootScope.posFull = lookToLoad.posFull;
                        // }, 3000);
                    });

                });

            });

            //////////////////////////////////////
        }
    });
