'use strict';

angular
    .module('mmOp')
    .component('siteFooter', {
        templateUrl: 'app/components/footer/footer.html',
        controller: function ($scope, $rootScope, $uibModal, $stateParams, AppConfig) {
            var $ctrl = this;

            $ctrl.contact = AppConfig.footer.contact;
            $ctrl.subject = AppConfig.footer.subject;

            var modals = {
                terms: {
                    size: 'lg',
                    templateUrl: 'app/components/footer/terms-modal.html',
                    cssClasses: '',
                    closeBtn: true
                },
                chartefb: {
                    size: 'md',
                    templateUrl: 'app/components/footer/chartefb-modal.html',
                    cssClasses: '',
                    closeBtn: true
                }
            };

            $ctrl.openModal = function(modalName){
                $uibModal.open({
                    animation: false,
                    templateUrl: modals[modalName].templateUrl,
                    size: modals[modalName].size,
                    controllerAs: 'modal',
                    controller: function(AppConfig){
                        var modal = this;
                        modal.activeModal = modals[modalName];
                        modal.client = AppConfig.footer.charteFb.client;
                        modal.contact = AppConfig.footer.contact;
                    }
                });
            };

            // monurl.com/#!/?modal=myModal opens 'myModal'
            if($stateParams.modal){
                $ctrl.openModal($stateParams.modal);
            }

            // $rootScope.$broadcast('openModal', 'myModal') anywhere opens myModal
            $rootScope.$on('openModal', function(e, modalName) {
                $ctrl.openModal(modalName);
            });
        }
    });
