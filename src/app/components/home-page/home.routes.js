'use strict';

angular
    .module('mmOp')
    .config(function ($stateProvider) {

        $stateProvider
            .state({
                name: 'app.home',
                url: '',
                component: 'homePage',
                data: {
                    pageId: 1
                },
                resolve: {
                    Identify: function(AppConfig, $resource, $cookies, $mmUser){
                        // Send Param. in the cookie Lacoste to the API for user identification

                        var cookieLogin = $cookies.get('eb-profile');
                        var loginParam = (cookieLogin) ? cookieLogin.split(':',1)[0].replace(/-/g, '') : '58a1c98ca8264b5e4b53110c';

                        $mmUser.set({
                            'loginParam': loginParam
                        });
                        if(!cookieLogin){
                            return $resource(AppConfig.getApiPrefix()+'/tracker/58a1c98ca8264b5e4b53110c/identify' )
                            .save({
                                'profile':{}
                            })
                            .$promise
                            .then(function(result){
                                // result - return obj "profile"
                                $mmUser.set(result);
                                return result; 
                            }).catch(function(){
                                // error
                            });
                        } else {
                            $mmUser.set({
                                profile: {
                                    id: loginParam 
                                }
                            });
                        }
                    }
                }
            });
    });
