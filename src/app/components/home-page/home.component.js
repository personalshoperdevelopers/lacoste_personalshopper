'use strict';

angular
    .module('mmOp')
    .component('homePage', {
        templateUrl: 'app/components/home-page/home.html',
        controller: function (AppConfig, $translate, $resource, $mmUser, $state, ImagesData, WidgetId, $rootScope, $mmTro) {
            var $ctrl = this;

            var lang = AppConfig.getLanguage();

            $ctrl.isClick = false;

            if($rootScope.currentPage == 1){
                $mmTro.addToQueue(
                    { 
                        _rtgpg: 'hp',
                        _rtgidcountry: lang.toUpperCase(),
                        _rtglanguage: (lang == 'gb') ? 'en' : 'fr',
                        _rtgstep: '1',
                        _rtglogged: '1'
                    }
                );
                $mmTro.sendRtg();
            }

            var widgetId = WidgetId[0]['fr']; // à remplacer par la bonne langue quand on aura les Ids...
            var profileId = ($mmUser.get().profile) ? $mmUser.get().profile.id : '';

            $ctrl.objImages = ImagesData[0];

            $ctrl.gender = ($mmUser.get().genderId) ? $mmUser.get().genderId : 1;
            $ctrl.event = ($mmUser.get().occasionId) ? $mmUser.get().occasionId : 1;

            if($mmUser.get().genderId == 3 || $mmUser.get().genderId == 4) $ctrl.event = 1;

            var aGender = ['man', 'woman', 'boy', 'girl'];
            var aEvent = ['whenever', 'weekend', 'sport', 'urban'];

            var aGenderLabel = [];
            var aEventLabel = [];

            // Taggage /////////////////////////

            var aGenderTaggage = ['Men', 'Women', 'Boys', 'Girls'];
            var aEventTaggage = ['AllOccasions', 'Weekend', 'Sport', 'City'];

            var currentLang = AppConfig.getLanguage();
            var today = new Date();
            today = today.toLocaleString().split(',');
            var currentDate = today[0] + '' + today[1];

            var utag_data = {
                page: {
                    language: currentLang,
                    pagecategory: 'Personnal Shopper',
                    pagename: 'Personnal Shopper/Gender Choice',
                    pageurl: 'https://www.lacoste.com/'+currentLang+'/',
                    sitecountrycode: currentLang.toUpperCase(),
                    sitecountryname: (currentLang == 'gb') ? '' : 'France',
                    subcategory: 'Gender Choice',
                    visitdate: currentDate // à voir format...
                },
                user: {
                    internalId: 'guest',
                    loggedin: 'false',
                    newsletterOptin: 'false',
                    usercountrycode: currentLang.toUpperCase(),
                    usercountryname: (currentLang == 'gb') ? '' : 'France',
                    usertype: 'guest'
                },
                personnalshopper: {
                    step: 1,
                    genderSelected: aGenderTaggage[($ctrl.gender-1)],
                    occasionselected: aEventTaggage[($ctrl.event-1)],
                    product: {}
                }
            }
            window.utag_data = utag_data;
            if(typeof Bootstrapper !== 'undefined') Bootstrapper.ensEvent.trigger("personnalShopper_changestep");
            ///////////////////////////////////

            $translate(['home.gender_1', 'home.gender_2', 'home.gender_3', 'home.gender_4', 'home.event_1', 'home.event_2', 'home.event_3', 'home.event_4']).then(function(result){
                
                aGenderLabel[0] = result['home.gender_1'];
                aGenderLabel[1] = result['home.gender_2'];
                aGenderLabel[2] = result['home.gender_3'];
                aGenderLabel[3] = result['home.gender_4'];

                aEventLabel[0] = result['home.event_1'];
                aEventLabel[1] = result['home.event_2'];
                aEventLabel[2] = result['home.event_3'];
                aEventLabel[3] = result['home.event_4'];

                $ctrl.genderLabel = aGenderLabel[($ctrl.gender-1)];
                $ctrl.eventLabel = aEventLabel[($ctrl.event-1)];

                $mmUser.set({
                    gender: aGender[($ctrl.gender-1)],
                    genderId: $ctrl.gender,
                    occasion: aEvent[($ctrl.event-1)],
                    occasionId: $ctrl.event
                });

                $rootScope.$broadcast('updateMenu');

                $ctrl.clickGenderChoice = function(){
                    if($ctrl.event) $ctrl.gender = null;
                };

                $ctrl.updateChoice = function(id){
                    $ctrl.gender = id;
                    $ctrl.genderLabel = aGenderLabel[($ctrl.gender-1)];

                    $mmUser.set({
                        genderId: $ctrl.gender
                    });

                    // Taggage /////////////////////////
                    utag_data = {
                        page: {
                            language: currentLang,
                            pagecategory: 'Personnal Shopper',
                            pagename: 'Personnal Shopper/Gender Choice',
                            pageurl: 'https://www.lacoste.com/'+currentLang+'/',
                            sitecountrycode: currentLang.toUpperCase(),
                            sitecountryname: (currentLang == 'gb') ? '' : 'France',
                            subcategory: 'Gender Choice',
                            visitdate: currentDate // à voir format...
                        },
                        user: {
                            internalId: 'guest',
                            loggedin: 'false',
                            newsletterOptin: 'false',
                            usercountrycode: currentLang.toUpperCase(),
                            usercountryname: (currentLang == 'gb') ? '' : 'France',
                            usertype: 'guest'
                        },
                        personnalshopper: {
                            step: 1,
                            genderSelected: aGenderTaggage[($ctrl.gender-1)],
                            occasionselected: aEventTaggage[($ctrl.event-1)],
                            product: {}
                        }
                    }
                    window.utag_data = utag_data;
                    if(typeof Bootstrapper !== 'undefined') Bootstrapper.ensEvent.trigger("personnalShopper_changestep");
                    ///////////////////////////////////

                    $rootScope.$broadcast('updateMenu');
                };

                $ctrl.clickEventChoice = function(){
                    $ctrl.event = null;
                };

                $ctrl.updateChoiceEvent = function(id){
                    $ctrl.event = id;
                    $ctrl.eventLabel = aEventLabel[($ctrl.event-1)];

                    $mmUser.set({
                        occasionId: $ctrl.event
                    });

                    // Taggage /////////////////////////
                    utag_data = {
                        page: {
                            language: currentLang,
                            pagecategory: 'Personnal Shopper',
                            pagename: 'Personnal Shopper/Occasion Choice',
                            pageurl: 'https://www.lacoste.com/'+currentLang+'/',
                            sitecountrycode: currentLang.toUpperCase(),
                            sitecountryname: (currentLang == 'gb') ? '' : 'France',
                            subcategory: 'Occasion Choice',
                            visitdate: currentDate // à voir format...
                        },
                        user: {
                            internalId: 'guest',
                            loggedin: 'false',
                            newsletterOptin: 'false',
                            usercountrycode: currentLang.toUpperCase(),
                            usercountryname: (currentLang == 'gb') ? '' : 'France',
                            usertype: 'guest'
                        },
                        personnalshopper: {
                            step: 2,
                            genderSelected: aGenderTaggage[($ctrl.gender-1)],
                            occasionselected: aEventTaggage[($ctrl.event-1)],
                            product: {}
                        }
                    }
                    window.utag_data = utag_data;
                    if(typeof Bootstrapper !== 'undefined') Bootstrapper.ensEvent.trigger("personnalShopper_changestep");
                    ///////////////////////////////////

                    $rootScope.$broadcast('updateMenu');
                };

                $ctrl.submitChoices = function(){

                    if(!$ctrl.isClick){

                        $ctrl.isClick = true;

                        if($mmUser.get().genderId == 3 || $mmUser.get().genderId == 4) $ctrl.event = 1;

                        $mmUser.set({
                            gender: aGender[($ctrl.gender-1)],
                            genderId: $ctrl.gender,
                            occasion: aEvent[($ctrl.event-1)],
                            occasionId: $ctrl.event
                        });

                        var paramId = widgetId.looks;

                        $resource(AppConfig.getApiPrefix()+'/widget/'+ paramId + '/recommendations/' + profileId )
                        .get({
                            variables:{
                                '$gender': aGender[($ctrl.gender-1)],
                                '$occasion': aEvent[($ctrl.event-1)]
                            }
                        })
                        .$promise
                        .then(function(result){
                            $mmUser.set(result);
                            // $mmUser.set({"id":"5d3202f0-f6f1-11e8-a348-d7c27bb7fcde","recommendations":[{"id":"LOOK-13","strategy":"local-mostPopular","score":1,"distribution":0,"product":{"_id":{"tenant":"58a1c98ca8264b5e4b5310fe","original_id":"LOOK-13"},"created":1543251616011,"updated":1543251616011,"recommended":true,"handmade":true,"related":{},"type":"look","image":"https://image1.lacoste.com/dw/image/v2/AAQM_PRD/on/demandware.static/Sites-FR-Site/Sites-master/fr/PH9427_EV0_20.jpg?","categories":["PS_LOOK","PS_LOOK_MAN","PS_LOOK_WEEKEND"]}},{"id":"LOOK-12","strategy":"local-mostPopular","score":1,"distribution":0,"product":{"_id":{"tenant":"58a1c98ca8264b5e4b5310fe","original_id":"LOOK-12"},"created":1543251616021,"updated":1543251616021,"recommended":true,"handmade":true,"related":{},"type":"look","image":"https://image1.lacoste.com/dw/image/v2/AAQM_PRD/on/demandware.static/Sites-FR-Site/Sites-master/fr/L1312_CC3_20.jpg?","categories":["PS_LOOK","PS_LOOK_MAN","PS_LOOK_WEEKEND"]}}],"pagination":{"total":2},"widget":{"title":"","attributes":{},"minResults":-1}});
                            // $state.go('app.qualif');
                            $rootScope.$broadcast('updatePage', {idPage:2});
                        }).catch(function(error){
                            $ctrl.isClick = false;
                        });
                    }

                };

                $ctrl.skipChoice = function(){

                    if(!$ctrl.isClick){

                        $ctrl.gender = Math.round(Math.random()*4) + 1;
                        $ctrl.event = Math.round(Math.random()*4) + 1;
                        
                        if($mmUser.get().genderId == 3 || $mmUser.get().genderId == 4) $ctrl.event = 1;

                        $mmUser.set({
                            gender: aGender[($ctrl.gender-1)],
                            genderId: $ctrl.gender,
                            occasion: aEvent[($ctrl.event-1)],
                            occasionId: $ctrl.event
                        });
                        var paramId = widgetId.looks;
                        $resource(AppConfig.getApiPrefix()+'/widget/'+ paramId + '/recommendations/' + profileId )
                        .get({
                            variables:{
                                '$gender': aGender[($ctrl.gender-1)],
                                '$occasion': aEvent[($ctrl.event-1)]
                            }
                        })
                        .$promise
                        .then(function(result){
                            $mmUser.set(result);
                            // $state.go('app.loading');
                            $rootScope.$broadcast('updatePage', {idPage:3});
                        }).catch(function(error){
                            $ctrl.isClick = false;
                        });
                    }

                };

            });

      
        }
    });
