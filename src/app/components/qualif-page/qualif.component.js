'use strict';

angular
    .module('mmOp')
    .component('qualifPage', {
        templateUrl: 'app/components/qualif-page/qualif.html',
        controller: function ($state, $mmUser, AppConfig, $resource, ImagesData, $rootScope, $mmTro) {
            var $ctrl = this;

            $ctrl.isClick = false;

            var profileId = ($mmUser.get().profile) ? $mmUser.get().profile.id : '';

            var lang = AppConfig.getLanguage();

            var aGenderTaggageMmtro = ['homme', 'femme', 'garçon', 'fille'];
            var aEventTaggageMmtro = ['toutes les occasions', 'week-end', 'sport', 'city'];

            if($rootScope.currentPage == 2){
                $mmTro.addToQueue(
                    { 
                        _rtgpg: 'preference',
                        _rtgidcountry: lang.toUpperCase(),
                        _rtglanguage: (lang == 'gb') ? 'en' : 'fr',
                        _rtgstep: '2',
                        _rtglogged: '1',
                        _rtglook_gender: aGenderTaggageMmtro[parseInt($mmUser.get().genderId)-1],
                        _rtglook_type: aEventTaggageMmtro[parseInt($mmUser.get().occasionId)-1]
                    }
                );
                $mmTro.sendRtg();
            }

            $ctrl.objImages = ImagesData[0];

            // Taggage /////////////////////////

            var currentLang = AppConfig.getLanguage();
            var today = new Date();
            today = today.toLocaleString().split(',');
            var currentDate = today[0] + '' + today[1];

            var aGenderTaggage = ['Men', 'Women', 'Boys', 'Girls'];
            var aEventTaggage = ['AllOccasions', 'Weekend', 'Sport', 'City'];

            var genderTaggage = aGenderTaggage[parseInt($mmUser.get().genderId)-1];
            var eventTaggage = aEventTaggage[parseInt($mmUser.get().occasionId)-1];

            var utag_data = {
                page: {
                    language: currentLang,
                    pagecategory: 'Personnal Shopper',
                    pagename: 'Personnal Shopper/Fashion evaluator "Tinder like"',
                    pageurl: 'https://www.lacoste.com/'+currentLang+'/',
                    sitecountrycode: currentLang.toUpperCase(),
                    sitecountryname: (currentLang == 'gb') ? '' : 'France',
                    subcategory: 'Fashion evaluator "Tinder like"',
                    visitdate: currentDate // à voir format...
                },
                user: {
                    internalId: 'guest',
                    loggedin: 'false',
                    newsletterOptin: 'false',
                    usercountrycode: currentLang.toUpperCase(),
                    usercountryname: (currentLang == 'gb') ? '' : 'France',
                    usertype: 'guest'
                },
                personnalshopper: {
                    step: 3,
                    genderSelected: genderTaggage,
                    occasionselected: eventTaggage,
                    product: {}
                }
            }
            window.utag_data = utag_data;
            if(typeof Bootstrapper !== 'undefined') Bootstrapper.ensEvent.trigger("personnalShopper_changestep");
            ///////////////////////////////////


            // INIT PROPOSALS
            $ctrl.nbReco = $mmUser.get().recommendations.length;
            $ctrl.aProposals = $mmUser.get().recommendations;

            $ctrl.currentProposal = 0;

            if($ctrl.aProposals.length > 0){

                if($ctrl.aProposals.length >= 1) $ctrl.aProposals[0].position = 1;
                if($ctrl.aProposals.length >= 2) $ctrl.aProposals[1].position = 2;
                if($ctrl.aProposals.length >= 3) $ctrl.aProposals[2].position = 3;

                $ctrl.bgImage = $ctrl.aProposals[$ctrl.currentProposal].product.image.split('?')[0];

            } else {
                $rootScope.$broadcast('updatePage', {idPage:3});
            }

            var aLike = [];
            $ctrl.countView = 0;

            $ctrl.dislike = function(id){

                if(!$ctrl.isClick){
                    $ctrl.isClick = true;
                    $resource(AppConfig.getApiPrefix()+'/tracker/58a1c98ca8264b5e4b53110c/activity')
                    .save({
                        'activity': {
                            'profile': profileId,
                            'verb': 'dislike',
                            'original_id': id
                        }
                    })
                    .$promise
                    .then(function(result){
                        var objMmtro = [{
                            _rtgpg: 'tinder_like',
                            _rtgidcountry: lang.toUpperCase(),
                            _rtglanguage: (lang == 'gb') ? 'en' : 'fr',
                            _rtgstep: '2',
                            _rtglogged: '1',
                            _rtgliked: '0',
                            _rtglook_nb: ($ctrl.countView+1)                            
                        }];
                        objMmtro[0]['_rtgidproduit_'+($ctrl.countView+1)+''] = id;
                        $mmTro.addToQueue(
                            objMmtro[0]
                        );
                        $mmTro.sendRtg();
                        var eventga = {
                            eventcategory: 'Personnal Shopper - fashion Evaluator',
                            eventaction: 'Unlike',
                            eventlabel: id
                        }
                        if(typeof dataLayer != 'undefined'){
                          dataLayer.push(eventga);
                        };
                        if(typeof Bootstrapper !== 'undefined') Bootstrapper.ensEvent.trigger("autoTrack_EventGA");
                        updateProposals();
                        $ctrl.isClick = false;
                    }).catch(function(error){
                        $ctrl.isClick = false;
                    });
                }
            };

            $ctrl.like = function(id){

                if(!$ctrl.isClick){
                    $ctrl.isClick = true;
                    $resource(AppConfig.getApiPrefix()+'/tracker/58a1c98ca8264b5e4b53110c/activity')
                    .save({
                        'activity': {
                            'profile': profileId,
                            'verb': 'like',
                            'original_id': id
                        }
                    })
                    .$promise
                    .then(function(result){
                        var objMmtro = [
                        { 
                            _rtgpg: 'tinder_like',
                            _rtgidcountry: lang.toUpperCase(),
                            _rtglanguage: (lang == 'gb') ? 'en' : 'fr',
                            _rtgstep: '2',
                            _rtglogged: '1',
                            _rtgliked: '1',
                            _rtglook_nb: ($ctrl.countView+1)
                        }];
                        objMmtro[0]['_rtgidproduit_'+($ctrl.countView+1)+''] = id;
                        $mmTro.addToQueue(
                            objMmtro[0]
                        );
                        $mmTro.sendRtg();
                        
                        aLike.push(id);
                        var eventga = {
                            eventcategory: 'Personnal Shopper - fashion Evaluator',
                            eventaction: 'Like',
                            eventlabel: id
                        }
                        if(typeof dataLayer != 'undefined'){
                          dataLayer.push(eventga);
                        };
                        if(typeof Bootstrapper !== 'undefined') Bootstrapper.ensEvent.trigger("autoTrack_EventGA");
                        
                        updateProposals();
                        $ctrl.isClick = false;
                    }).catch(function(error){
                        $ctrl.isClick = false;
                    });
                }
            };

            function updateProposals(){
                $ctrl.aProposals.shift();
                $ctrl.countView++;
                // REDIRECT RULES
                if($ctrl.countView >= 5 && aLike.length > 0){
                    $rootScope.$broadcast('updatePage', {idPage:3});
                } else if($ctrl.countView >= 15){
                    $rootScope.$broadcast('updatePage', {idPage:3});
                }

                if($ctrl.aProposals.length <= 0) {
                    $ctrl.goLook();
                    return false;
                }

                $ctrl.bgImage = $ctrl.aProposals[$ctrl.currentProposal].product.image.split('?')[0];

            }

            $ctrl.back = function(){
                var eventga = {
                    eventcategory: 'Personnal Shopper - Back bouton Step3',
                    eventaction: 'Click',
                    eventlabel: ''
                }
                if(typeof dataLayer != 'undefined'){
                  dataLayer.push(eventga);
                };
                if(typeof Bootstrapper !== 'undefined') Bootstrapper.ensEvent.trigger("autoTrack_EventGA");
                
                $rootScope.$broadcast('updatePage', {idPage:1});
            };

            $ctrl.goLook = function(){
                $rootScope.$broadcast('updatePage', {idPage:3});
            };
        }
    });
