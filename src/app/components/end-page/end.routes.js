'use strict';

angular
    .module('mmOp')
    .config(function ($stateProvider) {

        $stateProvider
            .state({
                name: 'app.end',
                url: 'end',
                component: 'endPage',
                data: {
                    // pageId: 3,
                    accessRules: {
                        loggedIn: false,
                        afterEnd: true
                    }
                }
                // resolve: {
                //   checkAccess: function(AppInitResolve, $mmDoorman, $transition$, $stateParams) {
                //     return $mmDoorman($transition$.to(), $stateParams);
                //   }
                // }
            });
    });
