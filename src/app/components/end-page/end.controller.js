'use strict';

angular
    .module('mmOp')
    .component('endPage', {
        templateUrl: 'app/components/end-page/end.html',
        controller: function (AppConfig, $uibModal) {
            var $ctrl = this;

            $ctrl.openModal = function(){
                $uibModal.open({
                    templateUrl: 'app/components/end-page/modal/winner-pop.html',
                    size: 'lg',
                    controllerAs: '$ctrl'
                });
            };

        }
    });
