'use strict';

angular
    .module('mmOp')
    .config(function ($stateProvider) {

        $stateProvider
            .state({
                name: 'error',
                url: '/error',
                component: 'errorPage'
            });
    });
