'use strict';

angular
    .module('mmOp')
    .component('errorPage', {
        templateUrl: 'app/components/error-page/error.html'
    });
