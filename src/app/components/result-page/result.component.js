'use strict';

angular
    .module('mmOp')
    .component('resultPage', {
        templateUrl: 'app/components/result-page/result.html',
        controller: function ($state, $scope, $mmUser, ImagesData, AppConfig, $resource, $timeout, WidgetId, $rootScope, $mmTro) {
            var $ctrl = this;

            $rootScope.currentTypeLook = 'top';

            var lang = AppConfig.getLanguage();

            var universes = [];
            var productLines = [];

            var currentLang = AppConfig.getLanguage();
            var today = new Date();
            today = today.toLocaleString().split(',');
            var currentDate = today[0] + '' + today[1];

            $ctrl.isMin = (window.outerWidth < 992) ? true : false;

            angular.element(window).resize(function(){
                $ctrl.isMin = (window.outerWidth < 992) ? true : false;
                $scope.$apply();
            });

            // Taggage /////////////////////////

            var aGenderTaggage = ['Men', 'Women', 'Boys', 'Girls'];
            var aEventTaggage = ['AllOccasions', 'Weekend', 'Sport', 'City'];

            var genderTaggage = aGenderTaggage[parseInt($mmUser.get().genderId)-1];
            var eventTaggage = aEventTaggage[parseInt($mmUser.get().occasionId)-1];

            var utag_data = {
                page: {
                    language: currentLang,
                    pagecategory: 'Personnal Shopper',
                    pagename: 'Personnal Shopper/Look proposal',
                    pageurl: 'https://www.lacoste.com/'+currentLang+'/',
                    sitecountrycode: currentLang.toUpperCase(),
                    sitecountryname: (currentLang == 'gb') ? '' : 'France',
                    subcategory: 'Look proposal',
                    visitdate: currentDate // à voir format...
                },
                user: {
                    internalId: 'guest',
                    loggedin: 'false',
                    newsletterOptin: 'false',
                    usercountrycode: currentLang.toUpperCase(),
                    usercountryname: (currentLang == 'gb') ? '' : 'France',
                    usertype: 'guest'
                },
                personnalshopper: {
                    step: 5,
                    genderSelected: genderTaggage,
                    occasionselected: eventTaggage,
                    product: {}
                }
            }
            window.utag_data = utag_data;
            if(typeof Bootstrapper !== 'undefined') Bootstrapper.ensEvent.trigger("personnalShopper_changestep");
            ///////////////////////////////////

            var widgetId = WidgetId[0]['fr'];

            var profileId = ($mmUser.get().profile) ? $mmUser.get().profile.id : '';

            var paramId = widgetId.products;
            if($mmUser.get().mainLook.recommendations.length > 0){
                $resource(AppConfig.getApiPrefix()+'/widget/'+ paramId +'/recommendations/'+ profileId)
                .get({
                    variables:{
                        $masterProductId: $mmUser.get().mainLook.recommendations[0].id,
                        $gender: $mmUser.get().gender,
                        $occasion: $mmUser.get().occasion,
                        $product_lines: productLines,
                        $universes: universes
                    }
                })
                .$promise
                .then(function(result){
                    $mmUser.set({
                        'fullLook': result
                    });
                    $ctrl.fullLook = result;
                });
            }


            $ctrl.objImages = ImagesData[0];

            $ctrl.getFullLook = function(id) {
                var widgetId = WidgetId[0]['fr'];
                var paramId = widgetId.products;
                return $resource(AppConfig.getApiPrefix()+'/widget/'+ paramId +'/recommendations/'+ profileId)
                    .get({
                        variables:{
                            $masterProductId: id,
                            $gender: $mmUser.get().gender,
                            $occasion: $mmUser.get().occasion,
                            $product_lines: productLines,
                            $universes: universes
                        }
                    })
                    .$promise
                    .then(function(result){
                        $rootScope.$broadcast('updateLook', id);
                        return result.recommendations;
                    });
            }

            $rootScope.$on('getFullLook', function(e, product, type){
                $rootScope.currentTypeLook = type;
                $ctrl.currentMaster = {
                    id: product._id.original_id,
                    product: product
                }
                $ctrl.changeMasterProduct($ctrl.currentMaster);
            });

            $ctrl.selectProducts = function(master, recommendations) {
                var date = new Date();
                var month = date.getMonth();
                var season  = (month == 10 || month == 11 || month == 0 || month == 1 || month == 2) ? 'WINTER' : 'SUMMER';
                var preTag = 'PS_' + $mmUser.get().gender.toUpperCase() + '_' + season + '_';

                var aTopProducts = $ctrl.getProductsByTag(master, recommendations, preTag+'TOP');
                var aBigProducts = $ctrl.getProductsByTag(master, recommendations, preTag+'BIG');

                
                // var aFinalTopProducts = $mmUser.get().mainLook.recommendations.map(function(row){
                //     return row.product;
                // });

                var aFinalTopProducts = [];
                for(var j = 0; j < aTopProducts.length; j++) if(!checkIsBig(aTopProducts[j]._id.original_id, aBigProducts)) aFinalTopProducts.push(aTopProducts[j]);
                

                return {
                    top: aFinalTopProducts,
                    bottom: $ctrl.getProductsByTag(master, recommendations, preTag+'BOTTOM'),
                    headAccessories: $ctrl.getProductsByTag(master, recommendations, preTag+'ACCESS_TOP'),
                    big: aBigProducts,
                    shoes: $ctrl.getProductsByTag(master, recommendations, preTag+'SHOES'),
                    accessories: $ctrl.getProductsByTag(master, recommendations, preTag+'ACCESS_SEASON'),
                    full: $ctrl.getProductsByTag(master, recommendations, preTag+'FULL'),
                    maincolor: (master.product.enrichment) ? master.product.enrichment.dominantColor : '#FFF'
                }
            }

            function checkIsBig(productId, aBigProducts){
                var isBig = false;
                for(var i = 0; i < aBigProducts.length; i++){
                    if (productId == aBigProducts[i]._id.original_id){
                        isBig = true;
                        break;
                    }
                };
                return isBig;
            }

            $ctrl.getProductsByTag = function(master, recommendations, tagName){
                var aProducts = [];
                for(var i = 0; i < master.product.categories.length; i++){
                    if(master.product.categories[i].search(tagName) > -1) aProducts.push(master.product);
                }
                // if(aMaster.length > 0) return aMaster;
                // var aProducts = [];
                for(var i = 0; i < recommendations.length; i++){
                    if(recommendations[i].product.categories){
                        for(var j = 0; j < recommendations[i].product.categories.length; j++){
                            if(recommendations[i].product.categories[j].search(tagName) > -1) aProducts.push(recommendations[i].product);
                        }
                    }
                }
                return aProducts;
            }


            $ctrl.changeMasterProduct = function(master){
                $ctrl.currentMaster = master;
                $rootScope.$broadcast('beforeLookChange');
                $ctrl.getFullLook($ctrl.currentMaster.id)
                    .then(function(recommendations) {
                        $ctrl.products = $ctrl.selectProducts($ctrl.currentMaster, recommendations);

                        $mmUser.set({
                            allProducts: $ctrl.products
                        });
                        
                        $ctrl.isTemplateA = ($ctrl.products.full.length > 0) ? false : true;
                        $rootScope.$broadcast('fullLookChange', $ctrl.products);
                        var stUniverses = '';
                        for(var i = 0; i < universes.length; i++){
                            if(universes[i] == 'FAS') stUniverses += 'Collection Défilé|';
                            if(universes[i] == 'LIV') stUniverses += 'Lacoste Live|';
                            if(universes[i] == 'SPO') stUniverses += 'Lacoste Sport|';
                            if(universes[i] == 'SPW') stUniverses += 'Lacoste|';
                        }
                        stUniverses = stUniverses.slice(0, -1);
                        var stProductLines = '';
                        for(var i = 0; i < productLines.length; i++){
                            if(productLines[i] == '1P') stProductLines += 'Polos|';
                            if(productLines[i] == '1T') stProductLines += 'Tshirts|';
                            if(productLines[i] == '1C') stProductLines += 'Shirts|';
                            if(productLines[i] == '1E') stProductLines += 'Dresses|';
                            if(productLines[i] == '1H') stProductLines += 'Pants|';
                            if(productLines[i] == '1W') stProductLines += 'Tracksuits|';
                            if(productLines[i] == '1G') stProductLines += 'Shorts|';
                            if(productLines[i] == '1J') stProductLines += 'Skirt|';
                            if(productLines[i] == '1Q') stProductLines += 'Tops|';
                        }
                        stProductLines = stProductLines.slice(0, -1);


                        if($rootScope.currentPage == 4) {

                            if($ctrl.products.full.length > 0){
                                $mmTro.addToQueue(
                                    { 
                                        _rtgpg: 'result',
                                        _rtgidcountry: lang.toUpperCase(),
                                        _rtglanguage: (lang == 'gb') ? 'en' : 'fr',
                                        _rtgstep: '3',
                                        _rtglogged: '1',
                                        _rtgidproduit_1: ($ctrl.products.full[0]) ? $ctrl.products.full[0]._id.original_id : '',
                                        _rtgidproduit_2: ($ctrl.products.headAccessories[0]) ? $ctrl.products.headAccessories[0]._id.original_id : '',
                                        _rtgidproduit_3: ($ctrl.products.big[0]) ? $ctrl.products.big[0]._id.original_id : '',
                                        _rtgidproduit_4: ($ctrl.products.shoes[0]) ? $ctrl.products.shoes[0]._id.original_id : '',
                                        _rtgidproduit_5: ($ctrl.products.accessories[0]) ? $ctrl.products.accessories[0]._id.original_id : '',
                                        _rtgidproduit_6: '',
                                        _rtgidcat_6: '',
                                        _rtgidsubcat_6: '',
                                        _rtgfilter_collection: stUniverses,
                                        _rtgfilter_products: stProductLines,
                                        _rtgfilter_colors: ''
                                    }
                                );
                                if($ctrl.products.full[0]){
                                    if($ctrl.products.full[0].cat) {
                                        $mmTro.addToQueue(
                                            { 
                                                _rtgidcat_1: $ctrl.products.full[0].cat.id,
                                                _rtgidsubcat_1: $ctrl.products.full[0].cat.id
                                            }
                                        );
                                    }
                                }
                                if($ctrl.products.headAccessories[0]){
                                    if($ctrl.products.headAccessories[0].cat) {
                                        $mmTro.addToQueue(
                                            { 
                                                _rtgidcat_2: $ctrl.products.headAccessories[0].cat.id,
                                                _rtgidsubcat_2: $ctrl.products.headAccessories[0].cat.id
                                            }
                                        );
                                    }
                                }
                                if($ctrl.products.big[0]){
                                    if($ctrl.products.big[0].cat) {
                                        $mmTro.addToQueue(
                                            { 
                                                _rtgidcat_3: $ctrl.products.big[0].cat.id,
                                                _rtgidsubcat_3: $ctrl.products.big[0].cat.id
                                            }
                                        );
                                    }
                                }
                                if($ctrl.products.shoes[0]){
                                    if($ctrl.products.shoes[0].cat) {
                                        $mmTro.addToQueue(
                                            { 
                                                _rtgidcat_4: $ctrl.products.shoes[0].cat.id,
                                                _rtgidsubcat_4: $ctrl.products.shoes[0].cat.id
                                            }
                                        );
                                    }
                                }
                                if($ctrl.products.accessories[0]){
                                    if($ctrl.products.accessories[0].cat) {
                                        $mmTro.addToQueue(
                                            { 
                                                _rtgidcat_5: $ctrl.products.accessories[0].cat.id,
                                                _rtgidsubcat_5: $ctrl.products.accessories[0].cat.id
                                            }
                                        );
                                    }
                                }
                            } else {                            
                                $mmTro.addToQueue(
                                    { 
                                        _rtgpg: 'result',
                                        _rtgidcountry: lang.toUpperCase(),
                                        _rtglanguage: (lang == 'gb') ? 'en' : 'fr',
                                        _rtgstep: '3',
                                        _rtglogged: '1',
                                        _rtgidproduit_1: ($ctrl.products.top[0]) ? $ctrl.products.top[0]._id.original_id : '',
                                        _rtgidproduit_2: ($ctrl.products.headAccessories[0]) ? $ctrl.products.headAccessories[0]._id.original_id : '',
                                        _rtgidproduit_3: ($ctrl.products.big[0]) ? $ctrl.products.big[0]._id.original_id : '',
                                        _rtgidproduit_4: ($ctrl.products.shoes[0]) ? $ctrl.products.shoes[0]._id.original_id : '',
                                        _rtgidproduit_5: ($ctrl.products.accessories[0]) ? $ctrl.products.accessories[0]._id.original_id : '',
                                        _rtgidproduit_6: ($ctrl.products.bottom[0]) ? $ctrl.products.bottom[0]._id.original_id : '',
                                        _rtgfilter_collection: stUniverses,
                                        _rtgfilter_products: stProductLines,
                                        _rtgfilter_colors: ''
                                    }
                                );
                                if($ctrl.products.top[0]){
                                    if($ctrl.products.top[0].cat) {
                                        $mmTro.addToQueue(
                                            { 
                                                _rtgidcat_1: $ctrl.products.top[0].cat.id,
                                                _rtgidsubcat_1: $ctrl.products.top[0].cat.id
                                            }
                                        );
                                    }
                                }
                                if($ctrl.products.headAccessories[0]){
                                    if($ctrl.products.headAccessories[0].cat) {
                                        $mmTro.addToQueue(
                                            { 
                                                _rtgidcat_2: $ctrl.products.headAccessories[0].cat.id,
                                                _rtgidsubcat_2: $ctrl.products.headAccessories[0].cat.id
                                            }
                                        );
                                    }
                                }
                                if($ctrl.products.big[0]){
                                    if($ctrl.products.big[0].cat) {
                                        $mmTro.addToQueue(
                                            { 
                                                _rtgidcat_3: $ctrl.products.big[0].cat.id,
                                                _rtgidsubcat_3: $ctrl.products.big[0].cat.id
                                            }
                                        );
                                    }
                                }
                                if($ctrl.products.shoes[0]){
                                    if($ctrl.products.shoes[0].cat) {
                                        $mmTro.addToQueue(
                                            { 
                                                _rtgidcat_4: $ctrl.products.shoes[0].cat.id,
                                                _rtgidsubcat_4: $ctrl.products.shoes[0].cat.id
                                            }
                                        );
                                    }
                                }
                                if($ctrl.products.accessories[0]){
                                    if($ctrl.products.accessories[0].cat) {
                                        $mmTro.addToQueue(
                                            { 
                                                _rtgidcat_5: $ctrl.products.accessories[0].cat.id,
                                                _rtgidsubcat_5: $ctrl.products.accessories[0].cat.id
                                            }
                                        );
                                    }
                                }
                                if($ctrl.products.bottom[0]){
                                    if($ctrl.products.bottom[0].cat) {
                                        $mmTro.addToQueue(
                                            { 
                                                _rtgidcat_6: $ctrl.products.bottom[0].cat.id,
                                                _rtgidsubcat_6: $ctrl.products.bottom[0].cat.id
                                            }
                                        );
                                    }
                                }
                            }
                            $mmTro.sendRtg();
                        }
                    })
            }

            $ctrl.back = function(){
                $rootScope.$broadcast('updatePage', {idPage:1});
            };


            if($mmUser.get().mainLook.recommendations.length > 0){
                
                $ctrl.hasResult = true;

                $ctrl.changeMasterProduct($mmUser.get().mainLook.recommendations[0]);
                
                
            } else {
                // 404 PAGE
                $ctrl.hasResult = false;
            }

            $rootScope.$on('masterProductChange', function(e, masterProduct, filterCollection){
                if(filterCollection) universes = filterCollection;
                $ctrl.changeMasterProduct(masterProduct);
            });

            $rootScope.$on('updateFiltersCollection', function(e, objFilters, noReload){
                var filters = objFilters.aUniverses;
                universes = [];
                if(filters[0]) universes.push('SPW');
                if(filters[1]) universes.push('LIV');
                if(filters[2]) universes.push('SPO');
                if(filters[3]) universes.push('FAS');
                if(!noReload) getMasters();
            });

            $rootScope.$on('updateFiltersProducts', function(e, objFilters, noReload){
                var filters = objFilters.aProductLines;
                productLines = [];

                if(filters[0]) productLines.push('1P'); // POLOS
                if(filters[1]) productLines.push('1T'); // T-SHIRT
                if(filters[2]) productLines.push('1C'); // SHIRTS
                if(filters[3]) productLines.push('1E'); // DRESSES
                if(filters[4]) productLines.push('1H'); // PANTALONS
                if(filters[5]) productLines.push('1W'); // SURVÊT
                if(filters[6]) productLines.push('1G'); // SHORTS
                if(filters[7]) productLines.push('1J'); // JUPES
                if(filters[8]) productLines.push('1Q'); // TOPS
                if(!noReload) getMasters();
            });

            function getMasters(){
                var paramId = widgetId.master;
                var objVariables = {
                    '$gender': $mmUser.get().gender,
                    '$occasion': $mmUser.get().occasion,
                    '$product_lines': productLines,
                    '$universes': universes                    
                }
                if(productLines.length < 1) delete objVariables['$product_lines'];
                if(universes.length < 1) delete objVariables['$universes'];

                $resource(AppConfig.getApiPrefix()+'/widget/'+ paramId +'/recommendations/'+ profileId)
                .get({
                    variables: objVariables                    
                })
                .$promise
                .then(function(result){
                    $mmUser.set({
                        'mainLook':result
                    });
                    if($mmUser.get().mainLook.recommendations.length > 1){
                        $ctrl.hasResult = true;
                        $ctrl.changeMasterProduct($mmUser.get().mainLook.recommendations[0]);                        
                    } else {
                        $ctrl.hasResult = false;
                    }
                }).catch(function(error){

                });
            }
        }
    });
