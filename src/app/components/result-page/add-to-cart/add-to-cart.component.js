'use strict';

angular
    .module('mmOp')
    .component('addToCart', {
        templateUrl: 'app/components/result-page/add-to-cart/add-to-cart.html',
        bindings:{
            pid: '<',
            color: '<'
        },
        controller: function ($mmTro, AppConfig, $rootScope) {
            var $ctrl = this;

            var lang = AppConfig.getLanguage();

            $ctrl.openCart = function(){
                if(typeof MqsPopin != 'undefined'){
                    var event = new CustomEvent('MQS:OPEN', {detail: {pid: $ctrl.pid, color: $ctrl.color}});
                    window.dispatchEvent(event);
                }
                if($rootScope.currentPage == 4) {

                    $mmTro.addToQueue(
                        { 
                            _rtgabanpan: '1',
                            _rtgidpdtaban: $ctrl.pid,                        
                            _rtgidcountry: lang.toUpperCase(),
                            _rtglanguage: (lang == 'gb') ? 'en' : 'fr',
                            _rtglogged: '1'
                        }
                    );
                    $mmTro.sendRtg();

                }
            }
        }
    });
    