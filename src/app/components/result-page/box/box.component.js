'use strict';

angular
    .module('mmOp')
    .component('box', {
        templateUrl: 'app/components/result-page/box/box.html',
        bindings:{
            products: '<',
            classes: '<',
            type: '<'
        },
        controller: function (ImagesData, $rootScope, $timeout, $mmUser) {
            var $ctrl = this;
            $ctrl.objImages = ImagesData[0];

            $ctrl.isMin = (window.with < 992) ? true : false;

            angular.element(window).resize(function(){
                $ctrl.isMin = (window.with < 992) ? true : false;
            });

            var isClick = false;

            $timeout(function(){
            
                $ctrl.prevSlick = function(tagSlick){
                    var slickC = angular.element('#slick'+tagSlick);
                    slickC.slick('slickPrev');
                    if(tagSlick == 'headAccessories'){
                        $rootScope.posHeadAccessories = slickC.slick('slickCurrentSlide');
                        updateAccessory(slickC.slick('slickCurrentSlide'));
                    }
                    if(tagSlick == 'big'){
                       $rootScope.posBig = slickC.slick('slickCurrentSlide');
                    }
                    if(tagSlick == 'top'){
                        $rootScope.posTop = slickC.slick('slickCurrentSlide');
                        if($rootScope.currentTypeLook == 'top'){
                            if($mmUser.get().allProducts.top.length > 1) {
                                $rootScope.$broadcast('getFullLook', $mmUser.get().allProducts.bottom[angular.element('#slickbottom').slick('slickCurrentSlide')], 'bottom');
                            }
                        }
                    }
                    if(tagSlick == 'bottom'){
                        $rootScope.posBottom = slickC.slick('slickCurrentSlide');
                        if($rootScope.currentTypeLook == 'bottom'){
                            if($mmUser.get().allProducts.bottom.length > 1) {
                                $rootScope.$broadcast('getFullLook', $mmUser.get().allProducts.top[angular.element('#slicktop').slick('slickCurrentSlide')], 'top');
                            }
                        }
                    }
                    if(tagSlick == 'accessories'){
                        $rootScope.posAccessories = slickC.slick('slickCurrentSlide');                       
                    }
                    if(tagSlick == 'shoes'){
                        $rootScope.posShoes = slickC.slick('slickCurrentSlide');
                        updateShoes(slickC.slick('slickCurrentSlide'));
                    }
                    if(tagSlick == 'full'){
                        $rootScope.posFull = slickC.slick('slickCurrentSlide');                       
                    }
                };
                $ctrl.nextSlick = function(tagSlick){
                    var slickC = angular.element('#slick'+tagSlick);
                    slickC.slick('slickNext');
                    if(tagSlick == 'headAccessories'){
                        $rootScope.posHeadAccessories = slickC.slick('slickCurrentSlide');  
                        updateAccessory(slickC.slick('slickCurrentSlide'));
                    }
                    if(tagSlick == 'big'){
                        $rootScope.posBig = slickC.slick('slickCurrentSlide');                        
                    }
                    if(tagSlick == 'top'){
                        $rootScope.posTop = slickC.slick('slickCurrentSlide'); 
                        if($rootScope.currentTypeLook == 'top'){
                            if($mmUser.get().allProducts.top.length > 1) {                              
                                $rootScope.$broadcast('getFullLook', $mmUser.get().allProducts.bottom[angular.element('#slickbottom').slick('slickCurrentSlide')], 'bottom');                      
                            }
                        }
                    }
                    if(tagSlick == 'bottom'){
                        $rootScope.posBottom = slickC.slick('slickCurrentSlide'); 
                        if($rootScope.currentTypeLook == 'bottom'){                            
                            if($mmUser.get().allProducts.bottom.length > 1) {                                
                                $rootScope.$broadcast('getFullLook', $mmUser.get().allProducts.top[angular.element('#slicktop').slick('slickCurrentSlide')], 'top');
                            }
                        }
                    }
                    if(tagSlick == 'accessories'){
                        $rootScope.posAccessories = slickC.slick('slickCurrentSlide');                        
                    }
                    if(tagSlick == 'shoes'){
                        $rootScope.posShoes = slickC.slick('slickCurrentSlide'); 
                        updateShoes(slickC.slick('slickCurrentSlide'));
                    }
                    if(tagSlick == 'full'){
                        $rootScope.posFull = slickC.slick('slickCurrentSlide');                        
                    }
                };

                $ctrl.prevSlickFull = function(){
                    var slickC = angular.element('#slickfull');
                    slickC.slick('slickPrev');
                    $rootScope.$broadcast('prevLookFull');
                }

                $ctrl.nextSlickFull = function(){
                    var slickC = angular.element('#slickfull');
                    slickC.slick('slickNext');
                    $rootScope.$broadcast('nextLookFull');
                }

                $rootScope.$on('fullLookChange', function(){
                    $timeout(function(){
                        var slickHA = angular.element('#slickheadAccessories'); 
                        var slickBig = angular.element('#slickbig'); 
                        var slickTop = angular.element('#slicktop'); 
                        var slickBottom = angular.element('#slickbottom'); 
                        var slickA = angular.element('#slickaccessories'); 
                        var slickShoes = angular.element('#slickshoes'); 
                        var slickFull = angular.element('#slickfull');

                        if(typeof slickHA != 'undefined') slickHA.slick('slickGoTo', $rootScope.posHeadAccessories, true);
                        if(typeof slickBig != 'undefined') slickBig.slick('slickGoTo', $rootScope.posBig, true);
                        if(typeof slickTop != 'undefined') slickTop.slick('slickGoTo', $rootScope.posTop, true);
                        if(typeof slickBottom != 'undefined') slickBottom.slick('slickGoTo', $rootScope.posBottom, true);
                        if(typeof slickA != 'undefined') slickA.slick('slickGoTo', $rootScope.posAccessories, true);
                        if(typeof slickShoes != 'undefined') slickShoes.slick('slickGoTo', $rootScope.posShoes, true);
                        if(typeof slickFull != 'undefined') slickFull.slick('slickGoTo', $rootScope.posFull, true);
                    }, 200);
                });

                function updateAccessory(indexAccessory){
                    $rootScope.$broadcast('updateHeadAccessories', {id: indexAccessory});
                }

                function updateShoes(indexShoes){
                    $rootScope.$broadcast('updateShoes', {id: indexShoes});
                }
            });
        }
    });
