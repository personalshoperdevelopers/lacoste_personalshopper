'use strict';

angular
    .module('mmOp')
    .component('templateA', {
        templateUrl: 'app/components/result-page/template-a/template-a.html',
        bindings:{
            config: '<'
        },
        controller: function ($rootScope, $timeout) {
            var $ctrl = this;
            $ctrl.initSlick = true;

            $ctrl.isMin = (window.outerWidth < 992) ? true : false;

            angular.element(window).resize(function(){
                $ctrl.isMin = (window.outerWidth < 992) ? true : false;
            });
            
            $rootScope.$on('beforeLookChange', function() {
                $ctrl.initSlick = false;
            });
            $rootScope.$on('fullLookChange', function(e, products){
                $ctrl.config = products;
                updateHeadAccessories(0);
                $timeout(function(){
                    $rootScope.$broadcast('updateShoes', {id: 0});
                }, 0);
                $ctrl.initSlick = true;
            });
            $rootScope.$on('updateHeadAccessories', function(e, obj){
                updateHeadAccessories(obj.id);
            });
            $rootScope.$on('updateShoes', function(e, obj){
                updateShoes(obj.id);
            });

            function updateHeadAccessories(id){
                $ctrl.currentHeadAccessories = ($ctrl.config) ? $ctrl.config.headAccessories[id] : null;
            }
            function updateShoes(id){
                $ctrl.currentShoes = ($ctrl.config) ? $ctrl.config.shoes[id] : null;
            }
        }
    });
