'use strict';

angular
    .module('mmOp')
    .directive('formGroup', function () {
        return {
            scope: {},
            restrict: 'C',
            link: function(scope, element){
                scope.domElement = element;
            },
            controller: function($scope, $element){
                this.setActive = function(){
                    $element.addClass('active');
                };
                this.removeActive = function(){
                    $element.removeClass('active');
                };
            }
        };
    })
    .directive('formControl', function ($timeout) {
        return {
            require: ['?^formGroup', 'ngModel'],
            scope: {
                ngModel: '='
            },
            restrict: 'C',
            link: function(scope, element, attrs, ctrl){
                if (!ctrl[0]) return;

                var formGroupCtrl = ctrl[0];
                var ngModel = ctrl[1];

                // ON RENDER IF AUTO FILL ––––––––––––––––––––––––––––––––––––––––––––––
                $timeout(function () {
                    if (ngModel.$viewValue) {
                        formGroupCtrl.setActive();
                    }
                }, 0);

                // CHECK IF VALUE, IF YES ADD ACTIVE –––––––––––––––––––––––––––––––––––
                var checkFocusAndValue = function checkFocusAndValue(value) {
                    if (value) {
                        formGroupCtrl.setActive();
                    }
                    return value;
                };
                ngModel.$formatters.push(checkFocusAndValue);
                ngModel.$parsers.push(checkFocusAndValue);

                // CHECK IF FOCUS / BLUR –––––––––––––––––––––––––––––––––––––––––––––––
                element.bind('focus',function () {
                    formGroupCtrl.setActive();
                });
                element.bind('blur', function () {
                    if (ngModel.$viewValue === undefined || ngModel.$viewValue === '') {
                        formGroupCtrl.removeActive();
                    }
                });
            }
        };
    });
