'use strict';

angular
    .module('mmOp')
    .directive('mmFbConnect', function(AppConfig, $resource, $facebook, $state, $mmUser, $filter){
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, element, attrs, ctrl) {
                var trackClicks = {};
                trackClicks.init = attrs.trackClickInit || 1;
                trackClicks.success = attrs.trackClickSuccess || 2;
                trackClicks.cancel = attrs.trackClickCancel || 3;

                element.bind('click', function(){
                    trackClick(trackClicks.init);

                    $facebook
                        .login()
                        .then(function(result){
                            if (result.status === 'connected') {
                                trackClick(trackClicks.success);
                                $facebook
                                    .api('/me?fields=gender,first_name,last_name,email')
                                    .then(socialAuth);
                            } else {
                                trackClick(trackClicks.cancel);
                            }
                        });
                });

                var trackClick = function(linkId){
                    var oTrackData = {
                        iIdU: +$mmUser.get().idU,
                        iPageId: $state.current.data.pageId,
                        iLinkNumber: linkId,
                        iVisitId: +$mmUser.get().visitId
                    };

                    $resource(AppConfig.getApiPrefix() +'/track/page_click')
                        .save({}, oTrackData);
                };

                var socialAuth = function(response) {
                    var socialAuthData = {
                        iPlatformId: 1,
                        iSocialUserId: +response.id,
                        sFirstName: response.first_name,
                        sLastName: response.last_name,
                        sEmail: response.email,
                        iCivilId: (response.gender === 'male') ? 1 : 2
                    };

                    $resource(AppConfig.getApiPrefix() +'/session/social_auth')
                        .save({}, socialAuthData)
                        .$promise
                        .then(function(result){
                            if(result.data[0].uid){
                                relog(result.data[0].uid, result.data[0].iPageId);
                            } else {
                                updateModel(response);
                            }
                        });
                };

                var relog = function(uid, iPageId) {
                    $resource(AppConfig.getApiPrefix() +'/session/relog/:uid')
                        .get({ uid: uid })
                        .$promise
                        .then(function(result){
                            var redirectTo = $filter('filter')($state.get(), {data:{ pageId: iPageId }})[0].name;

                            $mmUser.set(result.data[0]);
                            $mmUser.set({ isRelog: true });
                            $state.go(redirectTo);
                        });
                };

                var updateModel = function(response) {
                    ctrl.$viewValue.iPlatformId = 1;
                    ctrl.$viewValue.iSocialUserId = parseInt(response.id);
                    ctrl.$viewValue.sFirstName = response.first_name;
                    ctrl.$viewValue.sLastName = response.last_name;
                    ctrl.$viewValue.sEmail = response.email;
                    ctrl.$viewValue.iCivilId = (response.gender === 'male') ? 1 : 2;
                };
            }
        };
    });
