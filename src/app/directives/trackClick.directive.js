'use strict';

angular
    .module('mmOp')
    .directive('trackClick', function(AppConfig, $resource, $state, $mmUser, $mmTro) {
        return function(scope, element, attrs) {

            function getPageId(){
                if($state.current.data){
                    if($state.current.data.pageId){
                        return +$state.current.data.pageId;
                    }
                }
            }

            element.bind('click', function(){
                var oTrackData = {
                    iIdU: +$mmUser.get().idU,
                    iPageId: attrs.pageId || getPageId(),
                    iLinkNumber: +attrs.trackClick,
                    iVisitId: $mmUser.get().visitId || $mmUser.get().iVisitId
                };

                $resource(AppConfig.getApiPrefix() +'/track/page_click')
                    .save({}, oTrackData);
            });

            if (attrs.trotroId) {
                $mmTro
                    .addToQueue({ _rtgclickid: attrs.trotroId })
                    .sendRtg();
            }
        };
    });
