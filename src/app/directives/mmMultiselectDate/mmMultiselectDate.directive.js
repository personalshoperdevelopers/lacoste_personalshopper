/* eslint-disable */
'use strict';

angular
	.module('mmOp')
	.directive('multiSelectDate', function($filter) {
		return {
			restrict: 'AE',
			require: '?ngModel',
			replace: true,
      scope: {
        disabledDate:  '=',
        disabledDay:   '=',
        disabledMonth: '=',
        disabledYear:  '=',
        defaultDay:    '=',
        defaultMonth:  '=',
        defaultYear:   '='
      },
			// templateUrl: 'app/mmCore/directives/mmMultiselectDate/mmMultiselectDate.html',
			link: function(scope, element, attrs, ngModel) {
				if (!ngModel) return;

				// ATTRIBUTES (with default values if not set)
				var config = {
					maxYear: 		!!attrs.maxDate ? new Date(attrs.maxDate).getFullYear() 	: new Date().getFullYear(),
					maxMonth: 	!!attrs.maxDate ? new Date(attrs.maxDate).getMonth() +1	 	: 12,
					maxDay: 		!!attrs.maxDate ? new Date(attrs.maxDate).getDate()		 		: 31,
					minYear: 		!!attrs.minDate ? new Date(attrs.minDate).getFullYear() 	: new Date().getFullYear() - 100,
					minMonth: 	!!attrs.minDate ? new Date(attrs.minDate).getMonth() + 1 	: 1,
					minDay: 		!!attrs.minDate ? new Date(attrs.minDate).getDate()		 		: 1
				};
				scope.yearOrder = !!attrs.yearOrder && attrs.yearOrder === 'asc' ? false : true;

				// GET FROM NG MODEL AND PUT IT IN LOCAL SCOPE
				ngModel.$render = function() {
					scope.date = {
            day:    ngModel.$viewValue ? $filter('date')(ngModel.$viewValue, 'dd') : scope.defaultDay,
            month:  ngModel.$viewValue ? $filter('date')(ngModel.$viewValue, 'MM') : scope.defaultMonth,
            year:   ngModel.$viewValue ? $filter('date')(ngModel.$viewValue, 'yyyy') : scope.defaultYear
					};
				};

				// INIT YEARS, MONTHS AND DAYS NUMBER
				scope.selects = {

					// GEN YEARS
					years: function (){
						var yearsList = [];
						for (var i = config.maxYear; i >= config.minYear; i--) {
							yearsList.push(i.toString());
						}
						return yearsList;
					},

					// GEN MONTHS
					months: function(){
						var minMonth = !!scope.date.year && parseInt(scope.date.year) === config.minYear ? config.minMonth : 1;
						var maxMonth = !!scope.date.year && parseInt(scope.date.year) === config.maxYear ? config.maxMonth : 12;
						var monthList = [];

						for (var i = minMonth; i <= maxMonth; i++) {
							var iS = i.toString();
							monthList.push((iS.length < 2) ? '0' + iS : iS); // Adds a leading 0 if single digit
						}
						return monthList;
					},

					// GEN DAYS
					days: function(){
						var minDay = 	!!scope.date.year &&
													!!scope.date.month &&
													parseInt(scope.date.year) === config.minYear &&
													parseInt(scope.date.month) === config.minMonth
													? config.minDay : 1;

						var maxDay = 	!!scope.date.year &&
													!!scope.date.month &&
													parseInt(scope.date.year) === config.maxYear &&
													parseInt(scope.date.month) === config.maxMonth
													? config.maxDay : !!scope.date.year && !!scope.date.month ? new Date(scope.date.year, scope.date.month, 0).getDate() : 31;

						var daysList = [];
						for (var i = minDay; i <= maxDay; i++) {
							var iS = i.toString();
							daysList.push((iS.length < 2) ? '0' + iS : iS); // Adds a leading 0 if single digit
						}
						return daysList;
					}
				};

				// CHECK IF DAY IS CONSISTENT WITH CURRENT MONTH
				scope.isDayValid = function() {

					var scopeDate = new Date(scope.date.year + '-' + scope.date.month + '-' + scope.date.day);
					var calculateMonth = parseInt(scopeDate.toJSON().split('-')[1]);

					return parseInt(scope.date.month) === calculateMonth;
				};

				// WATCH FOR scope.date CHANGES
				scope.$watch('date', function(date) {

					// IF REQUIRED
					if (attrs.required) {

						// VALIDATION RULES
						var yearIsValid 	= !!date.year;
						var monthIsValid 	= !!date.month;
						var dayIsValid 		= !!date.day && scope.isDayValid();
						var allIsValid 		= !!date.year && !!date.month && (!!date.day && scope.isDayValid());

						// SET INPUT VALIDITY
						ngModel.$setValidity('required', 			allIsValid 		? true : false);
						ngModel.$setValidity('requiredYear', 	yearIsValid 	? true : false);
						ngModel.$setValidity('requiredMonth', monthIsValid 	? true : false);
						ngModel.$setValidity('requiredDay', 	dayIsValid 		? true : false);
					}

					// Need the 3 values to be filled to update the model
					if (date.year && date.month && date.day && allIsValid) {
						ngModel.$setViewValue(new Date(date.year, date.month - 1, date.day));
					}
				}, true);
			}
		};
	});
