'use strict';

// APP
angular
    .module('mmOp', [
        'ngAnimate',
        'ngCookies',
        // 'ngTouch',
        'ngSanitize',
        'ngResource',
        'ui.router',
        'ui.router.state.events',
        'ui.bootstrap',
        'ngStorage',
        'schemaForm',
        'angular-bind-html-compile',
        'ui.mask',
        'ngclipboard',
        'ngFacebook',
        'vcRecaptcha',
        'slick',
        'pascalprecht.translate'
    ]);

var getSiteEnv = function () {
    var actualEnv = [ ['local', 'localhost'], ['dev', 'mm-dev3.com'], ['staging', 'st1-mm.com'] ]
        .filter(function (env) { return document.location.hostname.indexOf(env[1]) > -1; });

    return actualEnv.length ? actualEnv[0][0] : 'production';
};