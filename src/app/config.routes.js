'use strict';

// ROUTES CONFIG
angular
    .module('mmOp')
    .config(function ($locationProvider, $urlRouterProvider, $logProvider, AppConfig) {

        $locationProvider
            .html5Mode(!!(window.history && window.history.pushState))
            .hashPrefix('!');

        $urlRouterProvider
            .when('', '/')
            .otherwise(function($injector) {
                var $state = $injector.get('$state');
                $state.go('app.home', null, {
                    location: false
                });
            });

        $logProvider.debugEnabled(AppConfig.env());
    })
    .run(function($state, $log, $transitions, $mmTro, $mmUser, AppConfig) {

        // ERROR HANDLERS
        $transitions.onError({}, function(trans){
            var err = trans.promise.$$state.value;

            var transitionErrorHandlers = {
                errorPage: function() {
                    $state.go('error');
                },
                accessRestriction: function() {
                    $log.debug('%cFORBIDDEN -> REDIRECT TO', 'font-weight: bold; text-transform: uppercase;', err);
                    $state.go(err.redirectTo, err.stateParams);
                },
                redirToIframe: function() {
                    document.location.href = AppConfig.redirectUrl;
                },
                reloadForIframe: function() {
                    $state.go(trans.to().name, err.stateParams);
                }
            };
            transitionErrorHandlers[err.type](); // Quicker than else if and switch
        });

        // MMTRO CONVERSION
        // $transitions.onSuccess({
        //     from: AppConfig.mmTroConversion.from,
        //     to: AppConfig.mmTroConversion.to
        // }, function(){
        //     if (!$mmUser.get().isRelog) {
        //         $mmTro.addToQueue({ _rtgconversion: 1 });
        //     }
        // });

        // SCROLL TOP
        $transitions.onSuccess({}, function(){
            window.scrollTo(0, 0);
        });
    });

// LANG CONFIG
var lang_fr = {
    "home":{
        "lookingFor": "Je cherche un look pour",
        "for": "pour",
        "gender_1": "un homme",
        "gender_2": "une femme",
        "gender_3": "un garçon",
        "gender_4": "une fille",
        "event_1": "toutes les occasions",
        "event_2": "week-end",
        "event_3": "sport",
        "event_4": "city",
        "btnNext": "suivant",
        "linkSkip": "Je souhaite voir directement les résultats, <u>surprenez-moi</u>."
    },
    "qualif":{
        "title": "Et j'aime",
        "btnPrevious": "retour",
        "btnDiscover": "je découvre mes looks"
    },
    "loading":{
        "loading": "Notre équipe<br>de stylistes s'active..."
    },
    "resultat":{
        "title": "Votre proposition de look",
        "sorry": "Désolé,<br>nous ne trouvons pas de look"
    },
    "menu":{
        "filter": "Filtrer par résultat",
        "collections": "Collections",
        "collect_1": "LACOSTE",
        "collect_2": "LACOSTE LIVE",
        "collect_3": "LACOSTE SPORT",
        "collect_4": "COLLECTION DÉFILÉ",
        "products": "Produits",
        "style_1": "POLOS",
        "style_2": "T-SHIRTS",
        "style_3": "CHEMISES",
        "style_4": "ROBES",
        "style_5": "PANTALONS",
        "style_6": "SURVÊTEMENTS",
        "style_7": "SHORTS",
        "style_8": "JUPES",
        "style_9": "HAUTS",
        "save_look": "ENREGISTRER CE LOOK",
        "your_looks": "VOS LOOKS",
        "lookingFor": "Je cherche un look",
        "for": "pour",
        "gender_1": "un homme",
        "gender_2": "une femme",
        "gender_3": "un garçon",
        "gender_4": "une fille",
        "event_1": "toutes les occasions",
        "event_2": "week-end",
        "event_3": "sport",
        "event_4": "city"
    }
}

var lang_en = {
    "home":{
        "lookingFor": "I want a look for",
        "for": "for",
        "gender_1": "a man",
        "gender_2": "a woman",
        "gender_3": "a boy",
        "gender_4": "a girl",
        "event_1": "any occasion",
        "event_2": "the weekend",
        "event_3": "sport",
        "event_4": "the city",
        "btnNext": "next",
        "linkSkip": "<u>Surprise me</u> - show me the looks"
    },
    "qualif":{
        "title": "And I like",
        "btnPrevious": "back",
        "btnDiscover": "Discover looks for me"
    },
    "loading":{
        "loading": "Our stylists<br>are creating your looks..."
    },
    "resultat":{
        "title": "Your suggested look",
        "sorry": "Sorry,<br>we can't find this look"
    },
    "menu":{
        "filter": "Filter by",
        "collections": "Collections",
        "collect_1": "LACOSTE",
        "collect_2": "LACOSTE LIVE",
        "collect_3": "LACOSTE SPORT",
        "collect_4": "COLLECTION DÉFILÉ",
        "products": "Products",
        "style_1": "POLOS",
        "style_2": "T-SHIRTS",
        "style_3": "SHIRTS",
        "style_4": "DRESSES",
        "style_5": "PANTS",
        "style_6": "TRACKSUITS",
        "style_7": "SHORTS",
        "style_8": "SKIRT",
        "style_9": "TOPS",
        "save_look": "SAVE THIS LOOK",
        "your_looks": "YOUR LOOKS",
        "lookingFor": "I want a look",
        "for": "for",
        "gender_1": "a man",
        "gender_2": "a woman",
        "gender_3": "a boy",
        "gender_4": "a girl",
        "event_1": "any occasion",
        "event_2": "the weekend",
        "event_3": "sport",
        "event_4": "the city"
    }
}

var lang_de = {
    "home":{
        "lookingFor": "Ich suche nach einem Look",
        "for": "für",
        "gender_1": "einen Mann",
        "gender_2": "eine Frau",
        "gender_3": "einen Jungen",
        "gender_4": "ein Mädchen",
        "event_1": "alle Anlässe",
        "event_2": "Wochenende",
        "event_3": "Sport",
        "event_4": "City",
        "btnNext": "weiter",
        "linkSkip": "Ich möchte die Ergebnisse direkt sehen. - <u>Überraschen Sie mich.</u>"
    },
    "qualif":{
        "title": "Und das gefällt mir",
        "btnPrevious": "zurück",
        "btnDiscover": "Meine looks ansehen"
    },
    "loading":{
        "loading": "Unser Stylisten-Team wird aktiv..."
    },
    "resultat":{
        "title": "Ihr Look-Vorschlag",
        "sorry": "Sorry,<br>wir können diesen Look nicht finden."
    },
    "menu":{
        "filter": "Filter nach Ergebnis",
        "collections": "Kollektionen",
        "collect_1": "LACOSTE",
        "collect_2": "LACOSTE LIVE",
        "collect_3": "LACOSTE SPORT",
        "collect_4": "COLLECTION DÉFILÉ",
        "products": "Produkte",
        "style_1": "POLOS",
        "style_2": "T-SHIRTS",
        "style_3": "SHIRTS",
        "style_4": "DRESSES",
        "style_5": "PANTS",
        "style_6": "TRACKSUITS",
        "style_7": "SHORTS",
        "style_8": "SKIRT",
        "style_9": "TOPS",
        "save_look": "DIESEN LOOK SPEICHERN",
        "your_looks": "IHRE LOOKS ",
        "lookingFor": "Ich suche nach einem Look",
        "for": "für",
        "gender_1": "einen Mann",
        "gender_2": "eine Frau",
        "gender_3": "einen Jungen",
        "gender_4": "ein Mädchen",
        "event_1": "alle Anlässe",
        "event_2": "Wochenende",
        "event_3": "Sport",
        "event_4": "City",
    }
}


angular
  .module('mmOp')
  .config(function (AppConfig, $translateProvider) {

    $translateProvider.useMissingTranslationHandlerLog();
    $translateProvider.translations('fr', lang_fr)
                      .translations('en', lang_en)
                      .translations('de', lang_de)

    // $translateProvider.useStaticFilesLoader({
    //   prefix:'../languages/lang_',
    //   suffix:'.json'
    // })
      // par défaut en fr
    var lang = "fr";

    $translateProvider.determinePreferredLanguage(function () {
      return AppConfig.getLanguage();
    });

    $translateProvider.useLocalStorage();

  });
