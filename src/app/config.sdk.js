'use strict';

// FACEBOOK CONFIG Don't forget to install ngFacebook: 'bower install --save ng-facebook'
angular
    .module('mmOp')
    .config(function (AppConfig, $facebookProvider, $compileProvider) {
        if(AppConfig.whatsapp.activateSdk){
            //other configuration code here
            $compileProvider.aHrefSanitizationWhitelist(/^\s*(whatsapp|http|https|mailto):/);
        }
        if (AppConfig.facebook.activateSdk) {
            var host = document.location.hostname;

            if (host.search('localhost') > -1) {
                $facebookProvider.setAppId(AppConfig.facebook.appIds.localhost);
            } else if (host.search('mm-dev3') > -1) {
                $facebookProvider.setAppId(AppConfig.facebook.appIds.dev);
            } else if (host.search('st1-mm') > -1) {
                $facebookProvider.setAppId(AppConfig.facebook.appIds.staging);
            } else {
                $facebookProvider.setAppId(AppConfig.facebook.appIds.prod);
            }

            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = '//connect.facebook.net/fr_FR/sdk.js';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        }
    });

// TWITTER CONFIG
angular
    .module('mmOp')
    .config(function (AppConfig) {

        if (AppConfig.twitter.activateSdk) {
            window.twttr = (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0],
                    t = window.twttr || {};
                if (d.getElementById(id)) return t;
                js = d.createElement(s);
                js.id = id;
                js.src = 'https://platform.twitter.com/widgets.js';
                fjs.parentNode.insertBefore(js, fjs);
                t._e = [];
                t.ready = function(f) { t._e.push(f); };
                return t;
            }(document, 'script', 'twitter-wjs'));
        }
    });
